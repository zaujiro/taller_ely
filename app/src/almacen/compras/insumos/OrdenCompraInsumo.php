<?php

namespace App\src\almacen\compras\insumos;

use Illuminate\Database\Eloquent\Model;

class OrdenCompraInsumo extends Model
{
    protected $table = 'ordenes_compras_insumos';
    protected $fillable = ['users_id','observaciones'];


    public function Insumos()
    {
        return $this->hasMany('App\src\almacen\compras\insumos\OrdenInsumo','ordenes_compras_insumos_id');
    }

    public  function User()
    {
        return $this->hasOne('App\User','id','users_id');
    }
}
