<?php

namespace App\src\almacen\compras\insumos;

use Illuminate\Database\Eloquent\Model;

class OrdenInsumo extends Model
{
    protected $table = 'ordenes_insumos';
    protected $fillable = ['almacen_insumos_id','ordenes_compras_insumos_id','precio','cantidad'];


    public function Insumo()
    {
        return $this->hasOne('App\src\almacen\configuracion\insumos\Insumo',
            'id', 'almacen_insumos_id');
    }
}
