<?php

namespace App\src\almacen\compras\productos;

use Illuminate\Database\Eloquent\Model;

/**
 * Modelo para el manejo de los productos de una orden de compra
 * Class OrdenProductos
 * @package App\src\almacen\compras\ordenes
 */
class OrdenProductos extends Model
{
    protected $table = 'ordenes_productos';
    protected $fillable = ['cantidad_solicitada', 'codigo', 'cantidad_recibida', 'precio_compra', 'precio_venta', 'precio_minimo', 'almacen_productos_proveedor_id', 'ordenes_compras_productos_id'];

    public function Proveedor()
    {
        return $this->hasOne('App\src\almacen\configuracion\proveedores\proveedores\Proveedor',
            'id','almacen_proveedores_id');
    }

    /**
     * Los productos de una orden son los que pertenecen al proveedor
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function ProductoProveedor()
    {
        return $this->hasOne('App\src\almacen\configuracion\proveedores\productos\ProductoProveedor', 'id', 'almacen_productos_proveedor_id');
    }
}
