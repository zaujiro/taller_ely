<?php

namespace App\src\almacen\compras\productos;

use Illuminate\Database\Eloquent\Model;

/**
 * Modelo para el manejo de las ordenes de compra
 * Class OrdenCompraProducto
 * @package App\src\almacen\compras\ordenes
 */
class OrdenCompraProducto extends Model
{

    protected $table = 'ordenes_compras_productos';
    protected $fillable = ['users_id', 'almacen_proveedores_id','fecha_orden', 'estado', 'credito', 'pagada', 'observaciones'];

    /**
     * Una orden de compra es realizada por un usuaior
     * relacion 1:1
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function User()
    {
        return $this->hasOne('App\User', 'id', 'users_id');
    }


    /**
     * Una orden de compra se realiza unicamente a un proveedor
     * relación 1:1
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function Proveedor()
    {
        return $this->hasOne('App\src\almacen\configuracion\proveedores\proveedores\Proveedor', 'id', 'almacen_proveedores_id');
    }

    /**
     * Una orden de compra posee varios productos
     * relación 1:N
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function Productos()
    {
        return $this->hasMany('App\src\almacen\compras\productos\OrdenProductos',  'ordenes_compras_productos_id');
    }
}
