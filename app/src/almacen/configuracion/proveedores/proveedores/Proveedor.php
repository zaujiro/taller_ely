<?php

namespace App\src\almacen\configuracion\proveedores\proveedores;

use Illuminate\Database\Eloquent\Model;

class Proveedor extends Model
{
    protected $table = 'almacen_proveedores';
    protected $fillable = ['nit','razon_social','telefono','celular','email','direccion','eliminado'];

    public function Productos()
    {
        return $this->hasMany('App\src\almacen\configuracion\proveedores\productos\ProductoProveedor',
            'almacen_proveedores_id');
    }
}
