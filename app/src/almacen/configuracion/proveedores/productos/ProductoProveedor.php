<?php

namespace App\src\almacen\configuracion\proveedores\productos;

use Illuminate\Database\Eloquent\Model;

class ProductoProveedor extends Model
{
    protected $table = 'almacen_productos_proveedor';
    protected $fillable = ['referencia','codigo','eliminado','almacen_productos_generales_id','almacen_proveedores_id'];

    public function Proveedor()
    {
        return $this->hasOne('App\src\almacen\configuracion\proveedores\proveedores\Proveedor',
            'id','almacen_proveedores_id');
    }

    public function ProductoGeneral()
    {
        return $this->hasOne('App\src\almacen\configuracion\productos\general\ProductosGeneral',
            'id','almacen_productos_generales_id');

    }
}
