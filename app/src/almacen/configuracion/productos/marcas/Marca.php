<?php

namespace App\src\almacen\configuracion\productos\marcas;

use Illuminate\Database\Eloquent\Model;

/**
 * Modelo para el manejo de las marcas de los productos
 * Class Marca
 * @package App\src\almacen\configuracion\marcas
 */
class Marca extends Model
{
    protected $table = 'almacen_marcas';
    protected $fillable = ['nombre','eliminado'];
}
