<?php

namespace App\src\almacen\configuracion\productos\materiales;

use Illuminate\Database\Eloquent\Model;

class Material extends Model
{
    protected $table = 'almacen_materiales';
    protected $fillable = ['nombre','eliminado'];
}
