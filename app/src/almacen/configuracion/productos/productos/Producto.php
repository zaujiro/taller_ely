<?php

namespace App\src\almacen\configuracion\productos\productos;

use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    protected $table = 'almacen_productos';
    protected $fillable = ['nombre', 'eliminado'];

    /**
     * Un producto tiene muchos productos en general
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function ProductosGeneral()
    {
        return $this->hasMany('App\src\almacen\configuracion\productos\general\ProductosGeneral',
            'almacen_productos_id');
    }

}
