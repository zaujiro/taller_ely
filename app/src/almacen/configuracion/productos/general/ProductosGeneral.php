<?php

namespace App\src\almacen\configuracion\productos\general;

use Illuminate\Database\Eloquent\Model;

/**
 * Modelo para los productos en general
 * que contiene el producto, la marca del producto, el material y la textura
 * Class ProductosGeneral
 * @package App\src\almacen\configuracion\productos\general
 */
class ProductosGeneral extends Model
{
    protected $table = 'almacen_productos_generales';
    protected $fillable = ['id', 'almacen_productos_id', 'almacen_marcas_id', 'almacen_materiales_id', 'referencia', 'eliminado'];


    /**
     * Un producto general tiene un solo Producto
     * relaciòn 1:1
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function Producto()
    {
        return $this->hasOne('App\src\almacen\configuracion\productos\productos\Producto', 'id', 'almacen_productos_id');
    }

    /**
     * Un producto general tiene una sola Marca
     * realciòn 1:1
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function Marca()
    {
        return $this->hasOne('App\src\almacen\configuracion\productos\marcas\Marca', 'id', 'almacen_marcas_id');
    }

    /**
     * Un producto general tiene solo un Material
     * relaciòn 1:1
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function Material()
    {
        return $this->hasOne('App\src\almacen\configuracion\productos\materiales\Material', 'id', 'almacen_materiales_id');
    }

}
