<?php

namespace App\src\almacen\configuracion\insumos;

use Illuminate\Database\Eloquent\Model;

class Insumo extends Model
{
    protected $table = 'almacen_insumos';
    protected $fillable = ['nombre','eliminado'];
}
