<?php

namespace App\src\sistema\menu;

use Illuminate\Database\Eloquent\Model;

/**
 * Módelo que contiene los datos de los menús
 * Class Menu
 * @package App\src\sistema\menu
 */
class Menu extends Model
{
    protected $table = 'menu';
    protected $fillable = ['id', 'nombre', 'src', 'orden', 'icon', 'id_padre'];

    /**
     * Un menú solo tiene un padre
     * relación 1:N
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function padre()
    {
        return $this->hasOne('App\src\sistema\menu\Menu','id','id_padre');
    }

    public function scopeSearch($query, $buscar)
    {
        return $query->where('nombre', 'LIKE', "%$buscar%")
            ->orWhere('src', 'LIKE', "%$buscar%");
    }
}
