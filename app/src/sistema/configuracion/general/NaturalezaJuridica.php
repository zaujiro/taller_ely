<?php

namespace App\src\sistema\configuracion\general;

use Illuminate\Database\Eloquent\Model;

/**
 * Clase que define la naturaleza de una entidad
 * Class NaturalezaJuridica
 * @package App\src\sistema\configuracion\general
 */
class NaturalezaJuridica extends Model
{
    protected $table = 'naturaleza_juridica';
    protected $fillable = ['nombre', 'eliminado'];
}
