<?php

namespace App\src\sistema\departamento;

use Illuminate\Database\Eloquent\Model;

class Departamentos extends Model
{
    protected $table = 'departamentos';
    protected $fillable = ['nombre', 'indicativo'];
}
