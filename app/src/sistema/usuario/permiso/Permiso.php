<?php

namespace App\src\sistema\usuario\permiso;

use Illuminate\Database\Eloquent\Model;

/**
 * Módelo que contiene los datos de los permisos
 * Class Permiso
 * @package App\src\sistema\usuario\permiso
 */
class Permiso extends Model
{
    protected $table = 'permisos';
    protected $fillable = ['id','ID_menu','ID_rol'];

    public function scopeSearch($query,$buscar)
    {
        return $query->join('roles', 'roles.id', '=','permisos.ID_rol')
            ->where('roles.nombre','LIKE',"%$buscar%");
    }

    /**
     * Un permiso solo tiene un rol
     * relación 1:N
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function rol()
    {
        return $this->hasOne('App\src\sistema\usuario\rol\Rol','id','ID_rol');
    }
}
