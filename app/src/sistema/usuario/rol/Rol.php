<?php

namespace App\src\sistema\usuario\rol;

use Illuminate\Database\Eloquent\Model;

/**
 * Módelo que contiene los datos de los roles
 * Class Rol
 * @package App\src\sistema\usuario\rol
 */
class Rol extends Model
{
    protected $table = 'roles';
    protected $fillable = ['nombre','habilitado'];

    public function scopeSearch($query,$buscar)
    {
        return $query->where('nombre','LIKE',"%$buscar%");
    }
}
