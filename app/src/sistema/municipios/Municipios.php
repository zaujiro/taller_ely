<?php

namespace App\src\sistema\municipios;

use Illuminate\Database\Eloquent\Model;

class Municipios extends Model
{
    protected $table = 'municipios';
    protected $fillable = ['id_departamento', 'nombre'];

    /**
     * Un municipio solo tiene un departamento
     * relación 1:N
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function departamento()
    {
        return $this->hasOne('App\src\sistema\departamento\Departamentos','id','id_departamento');
    }
}
