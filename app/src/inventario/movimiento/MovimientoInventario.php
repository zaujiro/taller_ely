<?php

namespace App\src\inventario\movimiento;

use Illuminate\Database\Eloquent\Model;

class MovimientoInventario extends Model
{
    protected $table = 'almacen_inventario_movimientos';
    protected $fillable = ['almacen_bodegas_id', 'almacen_productos_generales_id', 'cantidad', 'tipo', 'users_id'];

    /**
     * Un movimiento en inventario es realizado por un usuario
     * relacion 1:1
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function User()
    {
        return $this->hasOne('App\User', 'id', 'users_id');
    }

    public function ProductoGeneral()
    {
        return $this->hasOne('App\src\almacen\configuracion\productos\general\ProductosGeneral',
            'id', 'almacen_productos_generales_id');

    }
}
