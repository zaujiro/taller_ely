<?php

namespace App\src\inventario;

use Illuminate\Database\Eloquent\Model;

class Inventario extends Model
{
    protected $table = 'almacen_inventario';
    protected $fillable = ['almacen_bodegas_id', 'almacen_productos_generales_id','cantidad'];

    public function ProductoGeneral()
    {
        return $this->hasOne('App\src\almacen\configuracion\productos\general\ProductosGeneral',
            'id','almacen_productos_generales_id');

    }
}
