<?php

namespace App\src\inventario\configuracion;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Bodega
 * Modelo para el manejo de las bodegas de inventario
 * @package App\src\inventario\configuracion
 */
class Bodega extends Model
{
    protected $table = 'almacen_bodegas';
    protected $fillable = ['nombre', 'eliminado'];
}