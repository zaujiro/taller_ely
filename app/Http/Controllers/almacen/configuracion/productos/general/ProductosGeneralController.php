<?php

namespace App\Http\Controllers\almacen\configuracion\productos\general;

use App\src\almacen\configuracion\productos\general\ProductosGeneral;
use App\src\almacen\configuracion\productos\marcas\Marca;
use App\src\almacen\configuracion\productos\materiales\Material;
use App\src\almacen\configuracion\productos\productos\Producto;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductosGeneralController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $productosGeneral = ProductosGeneral::orderBy('almacen_productos_id', 'ASC')->paginate(20);
        return view('vendor.foods-online.almacen.configuracion.productos.general.index')->with('productosGeneral', $productosGeneral);
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $productos = Producto::where('eliminado', null)->orderBy('nombre', 'ASC')->pluck('nombre', 'id');
        $marcas = Marca::where('eliminado', null)->orderBy('nombre', 'ASC')->pluck('nombre', 'id');
        $materiales = Material::where('eliminado', null)->orderBy('nombre', 'ASC')->get();


        return view('vendor.foods-online.almacen.configuracion.productos.general.create')
            ->with('productos', $productos)
            ->with('marcas', $marcas)
            ->with('materiales', $materiales);
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /**
         * Leemos todos los materiales marcados
         */
        foreach ($request->almacen_materiales_id As $material_id) {
            /**
             * Creamos el objeto y lo guardamos en la bd
             */
            $productoGeneral = new ProductosGeneral();
            $productoGeneral->almacen_productos_id = $request->almacen_productos_id;
            $productoGeneral->almacen_marcas_id = $request->almacen_marcas_id;
            $productoGeneral->referencia = $request->referencia;
            $productoGeneral->almacen_materiales_id = $material_id;
            $productoGeneral->save();

        }
        flash('Se ha creado el producto correctamente', 'success');
        return redirect()->route('almacen.configuracion.general.productos.index');
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        dd("SHOW");
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        dd("EDIT");
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        dd("UPDATE");
        //
    }

    /**
     * Método que retorna los productos que ya han sido creados de una marca
     * @param $producto_id el id del producto que se va a crear
     * @param $marca_id el id de la marca que se va a crear
     * @return mixed una colección con los objetos de tipo producto que ya se han creado
     */
    public function getMateriales($producto_id, $marca_id)
    {
        /** Consultamos todos lo materiales disponibles **/
        $materiales = Material::where('eliminado', null)->orderBy('nombre', 'ASC')->get();

        /**
         * Consultamos los productos ya creados con la marca
         **/
        $productosGenerales = ProductosGeneral::where('almacen_productos_id', $producto_id)
            ->where('almacen_marcas_id', $marca_id)->get();

        /**
         * Buscamos los materiales que ya han sido seleccionados y creados para la marca
         */
        foreach ($materiales As $material) {
            /** Creamos un atributo para los materiales y saber si habilitamos el checkbox en la vista**/
            $material->habilitado = true;

            /** Si el material  está en la colección de los productos ya creados, cambiamos su valor a false **/
            if ($productosGenerales->where('almacen_materiales_id', $material->id)->count()) {
                $material->habilitado = false;
            }
        }

        return $materiales;
    }

    /**
     * Método que habilita un producto
     * actualiza el campo eliminado a null
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function habilitar($id)
    {
        $productoGeneral = ProductosGeneral::find($id);
        $productoGeneral->eliminado = null;
        $productoGeneral->save();

        flash('Se ha habilitado el producto correctamente', 'success');
        return redirect()->route('almacen.configuracion.general.productos.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $productoGeneral = ProductosGeneral::find($id);
        $productoGeneral->eliminado = 1;
        $productoGeneral->save();

        flash('Se ha eliminado el producto correctamente', 'danger');
        return redirect()->route('almacen.configuracion.general.productos.index');
        //
    }
}
