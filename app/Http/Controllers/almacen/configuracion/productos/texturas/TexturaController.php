<?php

namespace App\Http\Controllers\almacen\configuracion\productos\texturas;

use App\src\almacen\configuracion\productos\texturas\Textura;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TexturaController extends Controller
{
    public function index()
    {
       $texturas = Textura::where('eliminado',null) -> orderBy('nombre','ASC') -> paginate(20);
       return view('vendor.foods-online.almacen.configuracion.productos.texturas.index')->with('texturas',$texturas);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('vendor.foods-online.almacen.configuracion.productos.texturas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $texura = new Textura($request->all());
        $texura->save();
        flash("Se ha creado la textura correctamente",'success');
        return redirect() -> route('almacen.configuracion.productos.texturas.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $textura = Textura::find($id);
        return view('vendor.foods-online.almacen.configuracion.productos.texturas.edit') -> with('textura',$textura);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $textura = Textura::find($id);
        $textura -> fill($request->all());
        $textura -> save();
        flash("Se ha actualizado la textura correctamente",'success');
        return redirect() -> route('almacen.configuracion.productos.texturas.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $textura = Textura::find($id);
        $textura -> eliminado = 1;
        $textura -> save();
        flash("Se ha eliminado la textura correctamente",'danger');
        return redirect() -> route('almacen.configuracion.productos.texturas.index');
    }
}
