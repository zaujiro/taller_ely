<?php

namespace App\Http\Controllers\almacen\configuracion\productos\marcas;

use App\src\almacen\configuracion\productos\marcas\Marca;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MarcaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $marcas = Marca::where('eliminado', null)->orderBy('nombre', 'ASC')->paginate(20);

        return view('vendor.foods-online.almacen.configuracion.productos.marcas.index')->with('marcas', $marcas);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('vendor.foods-online.almacen.configuracion.productos.marcas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $marca = New Marca($request->all());
        $marca -> save();
        flash('Se ha creado la marca correctamente', 'success');
        return redirect()->route('almacen.configuracion.productos.marcas.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $marca = Marca::find($id);
        return view('vendor.foods-online.almacen.configuracion.productos.marcas.edit')->with('marca',$marca);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $marca = Marca::find($id);
        $marca->fill($request->all());
        $marca->save();
        flash('Se ha actualizado la marca correctamente', 'success');
        return redirect()->route('almacen.configuracion.productos.marcas.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $marca = Marca::find($id);
        $marca -> eliminado = 1;
        $marca->save();
        flash('Se ha eliminado la marca correctamente', 'danger');
        return redirect()->route('almacen.configuracion.productos.marcas.index');
    }
}
