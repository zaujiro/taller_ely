<?php

namespace App\Http\Controllers\almacen\configuracion\productos\productos;

use App\src\almacen\configuracion\productos\productos\Producto;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $productos = Producto::where('eliminado',null) -> orderBy('nombre','ASC') -> paginate(20);
        return view('vendor.foods-online.almacen.configuracion.productos.productos.index') -> with('productos',$productos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('vendor.foods-online.almacen.configuracion.productos.productos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $producto = New producto($request->all());
        $producto->save();
        flash("Se ha creado el producto correctamente",'success');
        return redirect() -> route('almacen.configuracion.productos.productos.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $producto = Producto::find($id);
        return view('vendor.foods-online.almacen.configuracion.productos.productos.edit') -> with('producto',$producto);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $producto = Producto::find($id);
        $producto->fill($request->all());
        $producto->save();
        flash("Se ha actualizado el producto correctamente",'success');
        return redirect() -> route('almacen.configuracion.productos.productos.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $producto = Producto::find($id);
        $producto -> eliminado = 1;
        $producto -> save();
        flash("Se ha eliminado el producto correctamente",'danger');
        return redirect() -> route('almacen.configuracion.productos.productos.index');
    }
}
