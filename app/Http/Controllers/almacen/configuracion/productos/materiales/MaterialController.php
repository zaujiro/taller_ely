<?php

namespace App\Http\Controllers\almacen\configuracion\productos\materiales;

use App\src\almacen\configuracion\productos\materiales\Material;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MaterialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $materiales = Material::Where('eliminado',null) -> orderBy('nombre','ASC') -> paginate(20);
        return view('vendor.foods-online.almacen.configuracion.productos.materiales.index') -> with('materiales',$materiales);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('vendor.foods-online.almacen.configuracion.productos.materiales.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $material = new Material($request->all());
        $material->save();
        flash('El material se ha creado correctamente','success');
        return redirect() -> route('almacen.configuracion.productos.materiales.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $material = Material::find($id);
        return view('vendor.foods-online.almacen.configuracion.productos.materiales.edit')->with('material',$material);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $material = Material::find($id);
        $material->fill($request->all());
        $material->save();
        flash("Se ha actualizado el material correctamente",'success');
        return redirect() -> route('almacen.configuracion.productos.materiales.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $material = Material::find($id);
        $material -> eliminado = 1;
        $material -> save();
        flash("Se ha eliminado el material correctamente",'danger');
        return redirect() -> route('almacen.configuracion.productos.materiales.index');
    }
}
