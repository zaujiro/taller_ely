<?php

namespace App\Http\Controllers\almacen\configuracion\proveedores\proveedores;

use App\src\almacen\configuracion\productos\productos\Producto;
use App\src\almacen\configuracion\proveedores\productos\ProductoProveedor;
use App\src\almacen\configuracion\proveedores\proveedores\Proveedor;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProveedorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $proveedores = Proveedor::where('eliminado', null)->orderBy('razon_social', 'ASC')->paginate(20);
        return view('vendor.foods-online.almacen.configuracion.proveedores.proveedores.index')
            ->with('proveedores', $proveedores);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $productos = Producto::where('eliminado', null)->orderBy('nombre', 'ASC')->get();

        return view('vendor.foods-online.almacen.configuracion.proveedores.proveedores.create')
            ->with('productos', $productos);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $proveedor = new Proveedor($request->all());
        $proveedor->save();

        /**
         * Leemos todos los productos marcados y los guardamos en la BD
         */
        foreach ($request->almacen_productos_generales_id As $key => $almacen_productos_generales_id) {
            $productoProveedor = new ProductoProveedor();
            $productoProveedor->almacen_productos_generales_id = $almacen_productos_generales_id;
            $productoProveedor->almacen_proveedores_id = $proveedor->id;
            $productoProveedor->referencia = $request->referencia[$key];
            $productoProveedor->save();
        }


        flash("Se ha creado el proveedores correctamente", 'success');
        return redirect()->route('almacen.configuracion.proveedores.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $proveedor = Proveedor::find($id);
        $productos = Producto::where('eliminado', null)->orderBy('nombre', 'ASC')->get();
        return view('vendor.foods-online.almacen.configuracion.proveedores.proveedores.edit')
            ->with('proveedor', $proveedor)
            ->with('productos', $productos);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
//        dd($request->all());
        $proveedor = Proveedor::find($id);
        $proveedor->fill($request->all());
        $proveedor->save();

        /**
         * Leemos todos los productos marcados y los guardamos en la BD
         */
        foreach ($request->almacen_productos_generales_id As $key => $almacen_productos_generales_id) {

            /**
             * Comprobamos si el producto marcado ya está creado
             */
            $productoProveedor = $proveedor->Productos->where('almacen_productos_generales_id', $almacen_productos_generales_id)->first();

//            dd($productoProveedor);
            /**
             * Si el producto estaba creado en la BD actualizamos el valor de la referencia por la del formualario
             */
            if ($productoProveedor) {
                $productoProveedor->referencia = $request->referencia[$key];
                $productoProveedor->save();
//                dd("SI");
            } /**
             *Si no se ha creado el producto para el proveedor, lo creamos y lo guardamos en la BD
             */
            else {

                $productoProveedor = new ProductoProveedor();
                $productoProveedor->almacen_productos_generales_id = $almacen_productos_generales_id;
                $productoProveedor->almacen_proveedores_id = $proveedor->id;
                $productoProveedor->referencia = $request->referencia[$key];
                $productoProveedor->save();

//                dd("NO");
            }
        }


        /**
         * Eliminamos todos los productos que no están en la lista de marcados
         */
        $productosNoMarcados = ProductoProveedor::where('almacen_proveedores_id', $id)
            ->whereNotIn('almacen_productos_generales_id', $request->almacen_productos_generales_id);
        $productosNoMarcados->delete();


        flash("Se ha actualizado el proveedor correctamente", 'success');
        return redirect()->route('almacen.configuracion.proveedores.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $proveedor = Proveedor::find($id);
        $proveedor->eliminado = 1;
        $proveedor->save();
        flash("Se ha eliminado el proveedor correctamente", 'danger');
        return redirect()->route('almacen.configuracion.proveedores.index');
    }
}
