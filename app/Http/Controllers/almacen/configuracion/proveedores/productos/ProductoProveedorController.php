<?php

namespace App\Http\Controllers\almacen\configuracion\proveedores\productos;

use App\src\almacen\configuracion\proveedores\productos\ProductoProveedor;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductoProveedorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $productos = ProductoProveedor::where('eliminado',null)->orderBy('almacen_proveedores_id','ASC')->paginate(20);
        return view('vendor.foods-online.almacen.configuracion.proveedores.productos.index')->with('productos',$productos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        dd('Te la creiste xD ');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $producto = ProductoProveedor::find($id);
        $producto -> eliminado = 1;
        $producto -> save();
        flash('El producto se ha eliminado correctamente','danger');
        return redirect()->route('almacen.configuracion.productosproveedor.index');
    }
}
