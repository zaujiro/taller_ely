<?php

namespace App\Http\Controllers\almacen\configuracion\insumos;

use App\src\almacen\configuracion\insumos\Insumo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class InsumoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $insumos = Insumo::where('eliminado',null) -> orderBy('nombre','ASC') -> paginate(20);
        return view('vendor.foods-online.almacen.configuracion.insumos.index')->with('insumos',$insumos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('vendor.foods-online.almacen.configuracion.insumos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $insumos = new Insumo($request->all());
        $insumos -> save();
        flash("Se ha creado el insumo correctamente",'success');
        return redirect() -> route('almacen.configuracion.insumos.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $insumo = Insumo::find($id);
        return view('vendor.foods-online.almacen.configuracion.insumos.edit')->with('insumo',$insumo);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $insumo = Insumo::find($id);
        $insumo -> fill($request->all());
        $insumo -> save();
        flash("El insumo se ha actualizado correctamente","success");
        return redirect() -> route("almacen.configuracion.insumos.index");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $insumo = Insumo::find($id);
        $insumo -> eliminado = 1;
        $insumo -> save();
        flash("El insumo se ha eliminado correctamente","danger");
        return redirect() -> route("almacen.configuracion.insumos.index");

    }
}
