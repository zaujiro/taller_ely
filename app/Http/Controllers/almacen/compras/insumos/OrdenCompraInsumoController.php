<?php

namespace App\Http\Controllers\almacen\compras\insumos;

use App\src\almacen\compras\insumos\OrdenCompraInsumo;
use App\src\almacen\compras\insumos\OrdenInsumo;
use App\src\almacen\configuracion\insumos\Insumo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class OrdenCompraInsumoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ordenes = OrdenCompraInsumo::orderBy('id','DES')->paginate(20);
        return view('vendor.foods-online.almacen.compras.insumos.index')->with('ordenes',$ordenes);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $insumos = Insumo::where('eliminado',null)->orderBy('nombre','ASC')->pluck('nombre','id');
        return view('vendor.foods-online.almacen.compras.insumos.create')->with('insumos',$insumos);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $orden = new OrdenCompraInsumo();
        $orden -> users_id = Auth::user()->id;;
        $orden -> save();


        foreach ($request -> insumo_id As $key => $insumo)
        {

            $ordenInsumo = new OrdenInsumo();
            $ordenInsumo -> almacen_insumos_id = $insumo;
            $ordenInsumo -> ordenes_compras_insumos_id = $orden -> id;
            $ordenInsumo -> precio = $request -> precio[$key];
            $ordenInsumo -> cantidad = $request -> cantidad[$key];
            $ordenInsumo -> save();
        }

        flash('Se ha agregado la orden correctamente','success');
        return redirect()->route('almacen.compras.insumos.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $orden = OrdenCompraInsumo::find($id);
        $insumos = OrdenInsumo::where('ordenes_compras_insumos_id',$id)->get();
        foreach ($insumos AS $insumo)
        {
            $insumo -> delete();
        }

        $orden -> delete();

        flash('Se ha eliminado la orden correctamente','danger');
        return redirect() -> route('almacen.compras.insumos.index');
    }
}
