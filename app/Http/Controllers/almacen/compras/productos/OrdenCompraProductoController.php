<?php

namespace App\Http\Controllers\almacen\compras\productos;

use App\src\almacen\compras\productos\OrdenCompraProducto;
use App\src\almacen\compras\productos\OrdenProductos;
use App\src\almacen\configuracion\proveedores\productos\ProductoProveedor;
use App\src\almacen\configuracion\proveedores\proveedores\Proveedor;
use App\src\empresa\Empresa;
use App\src\inventario\configuracion\Bodega;
use App\src\inventario\Inventario;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;

class OrdenCompraProductoController extends Controller
{
    /**
     * Método que muestra la lista de ordenes de compra de productos más reciente
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ordenes = OrdenCompraProducto::orderBy('fecha_orden', 'DESC')->paginate(20);
        return view('vendor.foods-online.almacen.compras.orden.index')
            ->with('ordenes', $ordenes);

    }

    /**
     * Muestra el formulario para la creación de una orden de compra de productos para el almacen
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $proveedores = Proveedor::where('eliminado', null)->pluck('razon_social', 'id');

        return view('vendor.foods-online.almacen.compras.orden.create')->with('proveedores', $proveedores);
    }

    /**
     * Crea una nueva orden de compra
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /**
         * Variable de control si se no se ha ingresado ninguna cantidad
         * nos permitirà eliminar la orden
         */
        $eliminarOrden = true;
        /** Obtenemos el usuario actual **/
        $userActual = Auth::user();

        /**
         * Creamos la orden de compra
         */
        $orden = new OrdenCompraProducto($request->all());
        $orden->users_id = $userActual->id;
        $orden->save();

        /**
         * Leemos los id de todos los productos que recibimos
         */
        foreach ($request->almacen_productos_proveedor_id As $key => $almacen_productos_proveedor_id) {

            /**
             * Comprobamos si las cantidades son mayor a cero
             */
            if ($request->cantidad[$key] > 0) {

                /**
                 * Consultamos los datos del producto del proveedor para traer algunos datos necesario a guardar,
                 * esto debido a que se pueden actualizar y no se deberían cambiar los registros ya generados
                 * como por ejemplo con el código del producto
                 */
                $productoProveedor = ProductoProveedor::find($almacen_productos_proveedor_id);

                /**
                 * Creamos entonces los productos de la orden de compra
                 */
                $ordenCompraProducto = new OrdenProductos();
                $ordenCompraProducto->ordenes_compras_productos_id = $orden->id;
                $ordenCompraProducto->almacen_productos_proveedor_id = $almacen_productos_proveedor_id;
                $ordenCompraProducto->cantidad_solicitada = $request->cantidad[$key];
                $ordenCompraProducto->referencia = $productoProveedor->referencia;
                $ordenCompraProducto->save();

                /**
                 * cambiamos el valor de la variable para no eliminar la orden
                 */
                $eliminarOrden = false;

            }
        }

        /**
         * Si no se ingresó alguna cantidad simplemente  no se crearon productos de esta orden, así que eliminamos la orden
         */
        if ($eliminarOrden) {
            $orden->delete();
        }

        flash("Se ha creado la orden de compra correctamente", 'success');
        return redirect()->route('compras.ordenes.index');
    }

    /**
     *Elimina toda una orden de compra incluido sus producto
     *
     * @param  int $id llave de la orden
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        /** Seleccionamos todos los productos de la orden y los eliminamos para no tener conflictos con las foraneas **/
        $productos = OrdenProductos::where('ordenes_compras_productos_id', $id);
        $productos->delete();

        /** Eliminamos la orden de compra **/
        $orden = OrdenCompraProducto::find($id);
        $orden->delete();

        flash('Se ha eliminado la orden de compra', 'danger');
        return redirect()->route('compras.ordenes.index');
    }

    /**
     * Muestra una lista con las ordenes de compra pendientes por recibir
     * @return $this
     */
    public function mostrarRecibir()
    {
        $ordenes = OrdenCompraProducto::where('estado', null)->orderBy('fecha_orden', 'DESC')->paginate(20);
        return view('vendor.foods-online.almacen.compras.orden.recibir.index')
            ->with('ordenes', $ordenes);
    }

    /**
     * Muestra el formulario para recibir los productos de una orden de compra
     * @param $id el id de la orden
     * @return $this
     */
    public function formularioRecibir($id)
    {
        $orden = OrdenCompraProducto::find($id);
        /**
         * Bodegas disponibles
         */
        $bodegas = Bodega::where('habilitado', null)->orderBy('nombre', 'ASC')->pluck('nombre', 'id');
        return view('vendor.foods-online.almacen.compras.orden.recibir.datos')
            ->with('bodegas', $bodegas)
            ->with('orden', $orden);
    }

    /**
     * Recibe las cantidades y los id de cada producto de la orden y actualiza en la BD
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function guardarRecibidoOrden(Request $request, $id)
    {
        /**
         * Ponemos el estado de anulado por defecto, si nunca hay productos recibidos
         * entonces se anulará toda la orden de compra
         */
        $estado = 2;

        $orden = OrdenCompraProducto::find($id);

        /**
         * Leemos los id de todos los productos
         */
        foreach ($request->id As $key => $id) {
            /**
             * Comprobamos si hay alguna cantidad para poner el estado de la orden en recibido, actualizar laas cantidades recibidas y
             * guardar el producto en inventario
             */
            if ($request->cantidad[$key] > 0) {
                $productoOrden = OrdenProductos::find($id);
                $productoOrden->cantidad_recibida = $request->cantidad[$key];
                $productoOrden->save();

                /**
                 * Como existe un producto que obtuvo cantidad recibida, el estado de la orden ya es recibido
                 */
                $estado = 1;

                /**
                 * Comprobamos si el producto ya se encuentra en inventario, de ser así solo debemos solo sumar
                 * las cantidades, de lo contrario debemos crear el producto
                 */

                $inventario = Inventario::where('almacen_productos_generales_id', $request->almacen_productos_generales_id[$key])
                    ->where('almacen_bodegas_id', $request->almacen_bodegas_id[$key])
                    ->first();


                if ($inventario) {
                    $inventario->cantidad = $inventario->cantidad + $request->cantidad[$key];

                } else {
                    $inventario = new Inventario();
                    $inventario->almacen_bodegas_id = $request->almacen_bodegas_id[$key];
                    $inventario->almacen_productos_generales_id = $request->almacen_productos_generales_id[$key];
                    $inventario->cantidad = $request->cantidad[$key];

                }

                $inventario->save();
//                dd($inventario);
            }
        }

        $orden->estado = $estado;
        $orden->save();

        flash('Orden recibida con exito', 'success');
        return redirect()->route('compras.ordenes.recibir.index');

    }

    /**
     * Genera un archivo PDF que se envia al proveedor
     * @param $id el id de la orden de compra
     * @return mixed el pdf que se genera
     */
    public function ordenPdf($id)
    {
        /** Consultamos los datos de la orden, ponemos get() y ponelos la condición para que traiga solo
         * la orden que estamos generando debido a que get() trae todos
         * para poder enviarla la vista pdf**/
        $orden = OrdenCompraProducto::find($id)->get()->where('id', $id);


        /** Consultamos los datos de la empresa para poner en la cabecera de los archivos pdf **/
        $empresa = Empresa::get();

        /** Creamos el archivo pdf **/
        $pdf = App::make('dompdf.wrapper');
        /** Cargamos la vista que contiene la estructura del pdf y enviamos los datos que necesitamos mostrar**/
        $pdf->loadView('vendor.foods-online.almacen.compras.orden.pdf.ordenPDF', ['empresa' => $empresa, 'orden' => $orden]);
        /** Retornamos el achivo pdf creado y damos el nombre con el que se va a descargar **/
        return $pdf->download('Orden de compra #' . $orden->first()->id . '.pdf');
        // Para cambiar a solo descarga utilizamos el método download en lugar del stream

//        return view('vendor.foods-online.almacen.compras.orden.pdf.ordenPDF')->with('empresa', $empresa)->with('orden', $orden);
    }


    /**
     * Retorna los producto de un proveedor
     * @param $id el id del proveedor
     * @return mixed la colección de productos del proveedor
     */
    public function getProductosProveedor($id)
    {
        $productosProveedor = ProductoProveedor::where('almacen_proveedores_id', $id)
            ->where('eliminado', null)->orderBy('referencia', 'ASC')->get();

        foreach ($productosProveedor As $productoProveedor) {
            $productoProveedor->marca = $productoProveedor->ProductoGeneral->Marca->nombre;
            $productoProveedor->material = $productoProveedor->ProductoGeneral->Material->nombre;
            $productoProveedor->producto = $productoProveedor->ProductoGeneral->Producto->nombre;
        }
        return $productosProveedor;
    }
}
