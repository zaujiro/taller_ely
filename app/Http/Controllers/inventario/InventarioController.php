<?php

namespace App\Http\Controllers\inventario;

use App\src\almacen\configuracion\productos\general\ProductosGeneral;
use App\src\inventario\configuracion\Bodega;
use App\src\inventario\Inventario;
use App\src\inventario\movimiento\MovimientoInventario;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class InventarioController extends Controller
{
    /**
     * Lista de bodegas en las que se tiene el inventario
     * @return $this
     */
    public function index()
    {
        /**
         * Seleccionamos solo las bodegas disponibles
         */
        $bodegas = Bodega::where('habilitado', null)->orderBy("nombre", 'ASC')->paginate(20);
        return view('vendor.foods-online.inventario.index')->with('bodegas', $bodegas);
    }


    /**
     * Muestra una lista con el inventario de la bodega seleccionada
     * @param $id el id de la bodega
     * @return $this
     */
    public function inventario($id)
    {
        $inventarios = Inventario::where('almacen_bodegas_id', $id)->paginate(20);
        return view('vendor.foods-online.inventario.inventario')->with('inventarios', $inventarios);
    }

    public function movimiento()
    {
        $bodegas = Bodega::where('habilitado', null)->orderBy('nombre', 'ASC')->pluck('nombre', 'id');
        $productos = ProductosGeneral::where('eliminado', null)->get();

        /**
         * Leemos los productos y creamos el pluck de los productos
         */
        foreach ($productos As $producto) {
            $productosPluck[$producto->id] = $producto->Producto->nombre . " - " . $producto->Marca->nombre . " - " . $producto->Material->nombre;

        }

        return view('vendor.foods-online.inventario.movimiento.index')
            ->with('productos', $productosPluck)
            ->with('bodegas', $bodegas);
    }

    /**
     * Realiza un movimiento en inventario (entrada / salida)
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function guardarMovimiento(Request $request)
    {
        /** Obtenemos el usuario actual **/
        $userActual = Auth::user();

        $valido = true;
        /**
         * Comprobamos si el producto ya se encuentra en inventario, de ser así solo debemos solo sumar o restar (dependiendo del tipo
         * de movimiento que se realice) las cantidades, de lo contrario debemos crear el producto
         */

        $inventario = Inventario::where('almacen_productos_generales_id', $request->almacen_productos_generales_id)
            ->where('almacen_bodegas_id', $request->almacen_bodegas_id)
            ->first();


        if ($inventario) {
            /**
             * Si el tipo de movimiento es 1, entonces se refiere a una entrada de inventario, de lo contrario es una salida
             */
            if ($request->tipo_movimiento == 1) {
                $inventario->cantidad = $inventario->cantidad + $request->cantidad;
            } else {

                if ($inventario->cantidad < $request->cantidad) {
                    $valido = false;
                    flash('No se realizó el movimiento en inventario. La cantidad disponible en inventario es '.$inventario->cantidad.' para este producto ', 'warning');

                } else {
                    $inventario->cantidad = $inventario->cantidad - $request->cantidad;
                }
            }
        } else {
            /**
             * Si el producto NO se encontraba en inventario y se quiere ingresar una salida, no creamos nada y enviamos una alerta
             */
            if ($request->tipo_movimiento == 2) {
                $valido = false;
                flash('No se puede realizar una salida de un producto no existente en inventario ', 'warning');

            } else {
                $inventario = new Inventario();
                $inventario->almacen_bodegas_id = $request->almacen_bodegas_id;
                $inventario->almacen_productos_generales_id = $request->almacen_productos_generales_id;
                $inventario->cantidad = $request->cantidad;
            }
        }

        if ($valido) {
            $inventario->save();

            /**
             * Creamos el movimiento a partir de lo que se hizo en inventario
             */
            $movimientoInventario = new MovimientoInventario($inventario->toArray());
            $movimientoInventario->users_id = $userActual->id;
            $movimientoInventario->tipo =$request->tipo_movimiento;
            $movimientoInventario->save();
            flash('Movimiento en inventario realizado', 'success');
        }

        return redirect()->route('inventario.movimiento');


    }
}
