<?php

namespace App\Http\Controllers\empresa;

use App\Http\Requests\empresa\EmpresaRequest;
use App\src\empresa\Empresa;
use App\src\sistema\departamento\Departamentos;
use App\src\sistema\municipios\Municipios;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EmpresaController extends Controller
{
    /**
     * Función para retornar los datos de la empresa
     */
    public function index()
    {
        /**
         * Consultamos los datos de la empresa y los enviamos a la vista
         */
        $empresa = Empresa::get();
        return view('vendor.foods-online.empresa.index')->with('empresaControl', $empresa);
    }

    /**
     * Función para crear una empresa
     * @return $this una vista para la creación de la nueva empresa
     */
    public function create()
    {
        /**
         * Se comprueba si ya existe un registro de empresa para redireccionar a la vista prinicipal
         */
        $empresa = Empresa::get()->count();

        if ($empresa > 0) {
            $this->index();
        }

        $departamentos = Departamentos::orderBy('nombre', 'ASC')->pluck('nombre', 'id');

        return view('vendor.foods-online.empresa.create')->with('departamentos', $departamentos);
    }

    /**
     * Función para guardar los datos de la empresa
     * se recibe la imagen, se renombre y se mueve a la carpeta correspondiente
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(EmpresaRequest $request)
    {
        //Manipulación de imageness
        if ($request->file('logo')) {
            $file = $request->file('logo');
            $name = 'logo_' . time() . '.' . $file->getClientOriginalExtension();
            $path = public_path() . '/img/foods-online/empresa/';
            $file->move($path, $name);

        }

        $empresa = new Empresa($request->all());
        $empresa->logo = $name;
        $empresa->save();
        flash('Se ha creado la organización correctamente', 'success');
        return redirect()->route('empresa.index');
    }

    /**
     * Función que retorna la vista para editar los datos de la empresa
     * se consulta el único registro de empresa que puede haber en la tabla de empresa
     * @param $id
     * @return $this
     */
    public function edit($id)
    {
        /**
         * Se consultan los datos de la empresa
         */
        //$empresa = Empresa::where('id_empresa',$id)->first();
        $empresa = Empresa::find($id);

        $departamentos = Departamentos::orderBy('nombre', 'ASC')->pluck('nombre', 'id');
        $municipios = Municipios::where('id_departamento', $empresa->municipio->departamento->id)
            ->orderBy('nombre', 'ASC')
            ->pluck('nombre', 'id');

        return view('vendor.foods-online.empresa.edit')
            ->with('empresa', $empresa)
            ->with('departamentos', $departamentos)
            ->with('municipios', $municipios);
    }


    /**
     * Función que actualiza los datos de la empresa en la base de datos
     * @param EmpresaRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(EmpresaRequest $request, $id)
    {
        $empresa = Empresa::find($id);
        $empresa->fill($request->all());

        //Manipulación de imageness
        if ($request->file('logo')) {
            $file = $request->file('logo');
            $name = 'logo_' . time() . '.' . $file->getClientOriginalExtension();
            $path = public_path() . '/img/foods-online/empresa/';
            $file->move($path, $name);

            /** Cambiamos el atributo logo de la empresa en caso de que se seleccione una nueva imagen */
            $empresa->logo = $name;
        }

        $empresa->save();

        flash('Se ha actualizado la organización correctamente', 'success');
        return redirect()->route('empresa.index');

    }

    /**
     * Función que selecciona los municipios de un departamento
     * @param $id el id del departamento
     * @return mixed una colección con los municipios
     */
    public function getMunicipios($id)
    {
        return Municipios::where('id_departamento', $id)->get();
    }
}
