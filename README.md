<b>Instalar:<b>
<ul>
    <li>PHPStorm</li>
    <li>Xampp</li>
    <li>Composer</li>
    <li>GIT controlador de versiones </li>
</ul>

Instala el xampp y después el composer.

Cuando clone el proyecto se mueve por consola a la ruta de la carpeta donde lo dejó, y ejecuta el siguiente comando  <b>composer install</b> y después <b>composer update</b>
el actualiza todas las librerias y rutas del proyecto; configura la Base de datos (xampp maneja phpmyadmin <a href="http://localhost/phpmyadmin/"> http://localhost/phpmyadmin</a>) el
archivo <b>.env</b> es el encargado de la configuración del motor de BD.

Cuando el proyecto esté configurado en la base de datos cambie el usuario que tiene sistemas@..... por el correo que quiera y en la contraseña le pega esto <b>$2y$10$sxjLUxnn81Uj/8qQbSrySOE8C/MmmebDfv5hQFSrx5Z/NZ0ImupDC</b> 
entonces cuando se vaya a logear pone el correo que haya puesto y la contraseña es <b>nose2017</b> y listo.





