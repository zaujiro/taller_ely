-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 15-11-2017 a las 02:34:05
-- Versión del servidor: 10.1.25-MariaDB
-- Versión de PHP: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `tallerely`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menu`
--

CREATE TABLE `menu` (
  `id` int(10) NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `src` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `orden` int(11) NOT NULL,
  `icon` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'fa fa-angle-right',
  `id_padre` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `menu`
--

INSERT INTO `menu` (`id`, `nombre`, `src`, `orden`, `icon`, `id_padre`, `created_at`, `updated_at`) VALUES
(0, 'SIN PADRE', '#', 0, '', -1, NULL, NULL),
(1, 'Configuración', '#', 8, 'fa fa-folder-open', 0, '2017-01-12 19:00:47', '2017-08-09 03:40:36'),
(2, 'Sistema', '#', 1, 'fa fa-angle-double-right', 1, '2017-01-12 19:28:25', '2017-01-12 21:10:53'),
(3, 'Menú', 'menu', 1, 'fa fa-angle-right', 2, '2017-01-12 20:26:47', '2017-01-18 19:01:17'),
(5, 'Roles', 'rol', 1, 'fa fa-angle-right', 10, '2017-01-12 21:13:55', '2017-01-18 19:08:16'),
(10, 'Usuarios', '#', 2, 'fa fa-angle-double-right', 1, '2017-01-13 21:09:04', '2017-01-18 19:01:30'),
(11, 'Usuario', 'user', 2, 'fa fa-angle-right', 10, '2017-01-13 21:23:02', '2017-01-18 19:00:49'),
(12, 'Permisos', 'permiso', 3, 'fa fa-angle-right', 10, '2017-01-18 18:58:39', '2017-01-18 19:48:33'),
(13, 'Organización', 'empresa', 4, 'fa fa-angle-right', 1, '2017-07-18 19:15:58', '2017-07-26 05:27:44'),
(14, 'Configuración almacén', '#', 1, 'fa fa-cogs', 0, '2017-10-26 06:33:27', '2017-10-26 06:33:27'),
(15, 'Productos', '#', 0, 'fa fa-angle-right', 14, '2017-10-26 06:34:22', '2017-10-26 06:34:22'),
(16, 'Marcas', 'almacen/configuracion/marcas', 0, 'fa fa-angle-right', 15, '2017-10-26 07:16:24', '2017-11-15 05:58:11'),
(17, 'Productos', 'almacen/configuracion/productos', 1, 'fa fa-angle-right', 15, '2017-11-15 03:14:42', '2017-11-15 03:14:42'),
(18, 'Texturas', 'almacen/configuracion/texturas', 3, 'fa fa-angle-right', 15, '2017-11-15 03:21:09', '2017-11-15 03:21:09'),
(19, 'Materiales', 'almacen/configuracion/materiales', 4, 'fa fa-angle-right', 15, '2017-11-15 03:21:57', '2017-11-15 03:21:57'),
(20, 'Proveedores', 'almacen/configuracion/proveedores', 2, 'fa fa-angle-right', 14, '2017-11-15 03:23:41', '2017-11-15 03:23:41'),
(21, 'General', 'almacen/configuracion/general/productos', 1, 'fa fa-angle-right', 15, '2017-11-15 03:38:06', '2017-11-15 04:04:52');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
