@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Actualizar insumo
@endsection


@section('main-content')

    <div class="panel panel-default">
        <div class="panel-body">
            <a href="{{route('almacen.configuracion.insumos.index')}}" class="btn btn-default">
                Volver
            </a>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">Actualizar insumo</div>
        <div class="panel-body">
            <div class="form-group">
                {!! Form::open(['route' => ['almacen.configuracion.insumos.update',$insumo], 'method'=>'PUT']) !!}

                <div class="form-group">
                    {!! Form::label('nombre','Nombre') !!}
                    {!! Form::text('nombre',$insumo->nombre,['class' => 'form-control', 'placeholder' => 'Nombre...', 'required']) !!}
                </div>

                <div class="form-group">
                    {!! Form::submit('Editar',['class' => 'btn btn-primary'])!!}
                </div>

                {!! Form::close() !!}
            </div>

        </div>
    </div>
@endsection


