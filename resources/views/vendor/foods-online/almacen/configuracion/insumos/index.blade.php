@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Insumos
@endsection


@section('main-content')
    <div class="panel panel-default">
        <div class="panel-heading">Insumos</div>
        <div class="panel-body">

            <a href="{{ route('almacen.configuracion.insumos.create') }}" class="btn btn-info">Registrar nuevo insumo</a>

            <!-- BUSCADOR DE TAGS -->
            {!! Form::open(['route' => 'almacen.configuracion.insumos.index', 'method' => 'GET', 'class' => 'navbar-form pull-right']) !!}
            <div class="input-group">
                {!! Form::text('buscar',null, ['class' => 'form-control', 'placeholder' => 'buscar insumo...', 'aria-describedby' => 'search']) !!}
                <span class="input-group-addon" id="search">
                    <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                </span>
            </div>
            {!! Form::close() !!}
            <!-- FIN DEL BUSCADOR -->

            @if(count($insumos)>0)
                <table class="table table-hover">
                    <thead>
                    <th>Nombre</th>
                    <th>Eliminado</th>
                    <th></th>
                    </thead>

                    <tbody>
                    @foreach($insumos As $insumo)
                    <tr>
                        <td>{{$insumo->nombre}}</td>
                        <td>{{$insumo->eliminado ? "SI": "NO"}}</td>
                        <td>
                            <a href="{{route('almacen.configuracion.insumos.edit',$insumo->id)}}" class="btn btn-warning">
                                <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                            </a>
                            <a href="{{route('almacen.configuracion.insumos.destroy',$insumo->id)}}" onclick=" return confirm('Desea eliminar este insumo?')" class="btn btn-danger">
                                <span class="glyphicon glyphicon-remove" aria-hidden="true">
                                </span>
                            </a>

                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
                {!! $insumos->render() !!}
                @else
                <hr>
                <p class="bg-warning text-center">
                    No se han registrado insumos
                </p>
            @endif

        </div>
    </div>
@endsection


