@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Marcas
@endsection


@section('main-content')
    <div class="panel panel-default">
        <div class="panel-heading">Marcas</div>
        <div class="panel-body">

            <a href="{{ route('orden') }}" class="btn btn-info">Registrar nueva marca</a>

            <!-- BUSCADOR DE TAGS -->
            {!! Form::open(['route' => 'almacen.configuracion.productos.marcas.index', 'method' => 'GET', 'class' => 'navbar-form pull-right']) !!}
            <div class="input-group">
                {!! Form::text('buscar',null, ['class' => 'form-control', 'placeholder' => 'buscar marca...', 'aria-describedby' => 'search']) !!}
                <span class="input-group-addon" id="search">
                    <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                </span>
            </div>
            {!! Form::close() !!}
            <!-- FIN DEL BUSCADOR -->

            @if(count($marcas)>0)
                <table class="table table-hover">
                    <thead>
                    <th>Nombre</th>
                    <th>Eliminado</th>
                    <th></th>
                    </thead>

                    <tbody>
                    @foreach($marcas As $marca)
                    <tr>
                        <td>{{$marca->nombre}}</td>
                        <td>{{$marca->eliminado ? "SI": "NO"}}</td>
                        <td>
                            <a href="{{route('orden',$marca->id)}}" class="btn btn-warning">
                                <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                            </a>
                            <a href="{{route('almacen.configuracion.productos.marcas.destroy',$marca->id)}}" onclick=" return confirm('Desea eliminar esta marca?')" class="btn btn-danger">
                                <span class="glyphicon glyphicon-remove" aria-hidden="true">
                                </span>
                            </a>

                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
                {!! $marcas->render() !!}
                @else
                <hr>
                <p class="bg-warning text-center">
                    No se han registrado marcas
                </p>
            @endif

        </div>
    </div>
@endsection


