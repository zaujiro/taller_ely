@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Productos
@endsection


@section('main-content')
    <div class="panel panel-default">
        <div class="panel-heading">Productos</div>
        <div class="panel-body">

            <a href="{{ route('almacen.configuracion.productos.productos.create') }}" class="btn btn-info">Registrar nuevo producto</a>

            <!-- BUSCADOR DE TAGS -->
            {!! Form::open(['route' => 'almacen.configuracion.productos.productos.index', 'method' => 'GET', 'class' => 'navbar-form pull-right']) !!}
            <div class="input-group">
                {!! Form::text('buscar',null, ['class' => 'form-control', 'placeholder' => 'Buscar producto...', 'aria-describedby' => 'search']) !!}
                <span class="input-group-addon" id="search">
                    <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                </span>
            </div>
            {!! Form::close() !!}
            <!-- FIN DEL BUSCADOR -->

            @if(count($productos)>0)
                <table class="table table-hover">
                    <thead>
                    <th>Nombre</th>
                    <th>Eliminado</th>
                    <th></th>
                    </thead>

                    <tbody>
                    @foreach($productos As $producto)
                    <tr>
                        <td>{{$producto->nombre}}</td>
                        <td>{{$producto->eliminado ? "SI": "NO"}}</td>
                        <td>
                            <a href="{{route('almacen.configuracion.productos.productos.edit',$producto->id)}}" class="btn btn-warning">
                                <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                            </a>
                            <a href="{{route('almacen.configuracion.productos.productos.destroy',$producto->id)}}" onclick=" return confirm('Desea eliminar este producto?')" class="btn btn-danger">
                                <span class="glyphicon glyphicon-remove" aria-hidden="true">
                                </span>
                            </a>

                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
                {!! $productos->render() !!}
                @else
                <hr>
                <p class="bg-warning text-center">
                    No se han registrado productos
                </p>
            @endif
        </div>
    </div>
@endsection


