@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Actualizar producto
@endsection


@section('main-content')

    <div class="panel panel-default">
        <div class="panel-body">
            <a href="{{route('almacen.configuracion.productos.productos.index')}}" class="btn btn-default">
                Volver
            </a>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">Actualizar producto</div>
        <div class="panel-body">
            <div class="form-group">
                {!! Form::open(['route' => ['almacen.configuracion.productos.productos.update',$producto], 'method' => 'PUT']) !!}

                <div class="form-group">
                    {!! Form::label('nombre','Nombre') !!}
                    {!! Form::text('nombre',$producto->nombre,['class' => 'form-control', 'placeholder' => 'Nombre...', 'required']) !!}
                </div>

                <div class="form-group">
                    {!! Form::submit('Editar',['class' => 'btn btn-primary'])!!}
                </div>

                {!! Form::close() !!}
            </div>

        </div>
    </div>
@endsection