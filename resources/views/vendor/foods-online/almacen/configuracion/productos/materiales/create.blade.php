@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Nuevo material
@endsection


@section('main-content')

    <div class="panel panel-default">
        <div class="panel-body">
            <a href="{{route('almacen.configuracion.productos.materiales.index')}}" class="btn btn-default">
                Volver
            </a>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">Crear material</div>
        <div class="panel-body">
            <div class="form-group">
                {!! Form::open(['route' => 'almacen.configuracion.productos.materiales.store', 'method' => 'POST']) !!}

                <div class="form-group">
                    {!! Form::label('nombre','Nombre') !!}
                    {!! Form::text('nombre',null,['class' => 'form-control', 'placeholder' => 'Nombre...', 'required']) !!}
                </div>

                <div class="form-group">
                    {!! Form::submit('Registrar',['class' => 'btn btn-primary'])!!}
                </div>

                {!! Form::close() !!}
            </div>

        </div>
    </div>
@endsection


