@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Materiales
@endsection


@section('main-content')
    <div class="panel panel-default">
        <div class="panel-heading">Materiales</div>
        <div class="panel-body">

            <a href="{{ route('almacen.configuracion.productos.materiales.create') }}" class="btn btn-info">Registrar nuevo material</a>

            <!-- BUSCADOR DE TAGS -->
            {!! Form::open(['route' => 'almacen.configuracion.productos.materiales.index', 'method' => 'GET', 'class' => 'navbar-form pull-right']) !!}
            <div class="input-group">
                {!! Form::text('buscar',null, ['class' => 'form-control', 'placeholder' => 'Buscar material...', 'aria-describedby' => 'search']) !!}
                <span class="input-group-addon" id="search">
                    <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                </span>
            </div>
            {!! Form::close() !!}
            <!-- FIN DEL BUSCADOR -->

            @if(count($materiales)>0)
                <table class="table table-hover">
                    <thead>
                    <th>Nombre</th>
                    <th>Eliminado</th>
                    <th></th>
                    </thead>

                    <tbody>
                    @foreach($materiales As $material)
                    <tr>
                        <td>{{$material->nombre}}</td>
                        <td>{{$material->eliminado ? "SI": "NO"}}</td>
                        <td>
                            <a href="{{route('almacen.configuracion.productos.materiales.edit',$material->id)}}" class="btn btn-warning">
                                <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                            </a>
                            <a href="{{route('almacen.configuracion.productos.materiales.destroy',$material->id)}}"
                                onclick=" return confirm('Desea eliminar este material?')" class="btn btn-danger">
                                <span class="glyphicon glyphicon-remove" aria-hidden="true">
                                </span>
                            </a>

                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
                {!! $materiales->render() !!}
                @else
                <hr>
                <p class="bg-warning text-center">
                    No se han registrado materiales
                </p>
            @endif
        </div>
    </div>
@endsection


