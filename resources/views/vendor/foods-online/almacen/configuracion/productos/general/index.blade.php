@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Productos Generales
@endsection


@section('main-content')
    <div class="panel panel-default">
        <div class="panel-heading">Productos generales</div>
        <div class="panel-body">

            <a href="{{ route('almacen.configuracion.general.productos.create') }}" class="btn btn-info">Registrar nuevo
                producto</a>

            <!-- BUSCADOR DE TAGS -->
            {!! Form::open(['route' => 'almacen.configuracion.general.productos.index', 'method' => 'GET', 'class' => 'navbar-form pull-right']) !!}
            <div class="input-group">
                {!! Form::text('buscar',null, ['class' => 'form-control', 'placeholder' => 'buscar producto...', 'aria-describedby' => 'search']) !!}
                <span class="input-group-addon" id="search">
                    <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                </span>
            </div>
            {!! Form::close() !!}
        <!-- FIN DEL BUSCADOR -->

            @if(count($productosGeneral)>0)
                <table class="table table-hover">
                    <thead>
                    <th>Producto</th>
                    <th>Marca</th>
                    <th>Material</th>
                    <th>Referencia</th>
                    <th>Eliminado</th>
                    <th></th>
                    </thead>

                    <tbody>
                    @foreach($productosGeneral As $productoGeneral)
                        <tr>
                            <td>{{$productoGeneral->Producto->nombre}}</td>
                            <td>{{$productoGeneral->Marca->nombre}}</td>
                            <td>{{$productoGeneral->Material->nombre}}</td>
                            <td>{{$productoGeneral->referencia ? $productoGeneral->referencia : " - "}}</td>
                            <td>{{$productoGeneral->eliminado ? "SI": "NO"}}</td>
                            <td>
                                <!-- Si el producto está eliminado mostramos el botón para habilitar, de lo contrario el de eliminar -->
                                @if($productoGeneral->eliminado)
                                    <a href="{{route('almacen.configuracion.general.productos.habilitar',$productoGeneral->id)}}"
                                       onclick=" return confirm('Desea habilitar este producto?')"
                                       class="btn btn-success">
                                        <span class="glyphicon glyphicon-ok" aria-hidden="true">
                                        </span>
                                    </a>
                                    @else
                                    <a href="{{route('almacen.configuracion.general.productos.destroy',$productoGeneral->id)}}"
                                       onclick=" return confirm('Desea eliminar este producto?')"
                                       class="btn btn-danger">
                                        <span class="glyphicon glyphicon-remove" aria-hidden="true">
                                        </span>
                                    </a>
                                @endif

                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {!! $productosGeneral->render() !!}
            @else
                <hr>
                <p class="bg-warning text-center">
                    No se han registrado productos
                </p>
            @endif

        </div>
    </div>
@endsection


