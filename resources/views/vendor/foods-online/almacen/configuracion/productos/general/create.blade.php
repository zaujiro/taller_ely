@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Nuevo producto general
@endsection


@section('main-content')

    <div class="panel panel-default">
        <div class="panel-body">
            <a href="{{route('almacen.configuracion.general.productos.index')}}" class="btn btn-default">
                Volver
            </a>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">Crear producto general</div>
        <div class="panel-body">
            {!! Form::open(['route' => 'almacen.configuracion.general.productos.store', 'method' => 'POST']) !!}

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        {!! Form::label('almacen_productos_id','Producto') !!}
                        {!! Form::select('almacen_productos_id',$productos,null,['class' => 'form-control select-padre','id'=>'select-producto','placeholder' => 'seleccione una opción', 'required']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('almacen_marcas_id','Marca') !!}
                        {!! Form::select('almacen_marcas_id',$marcas,null,['class' => 'form-control select-padre','id' => 'select-marca','placeholder' => 'seleccione una opción', 'required']) !!}
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Materiales
                        </div>
                        <div class="panel-body">
                            <label id="label-materiales">
                                @foreach($materiales As $material)
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="#" value="#">
                                            {{$material->nombre}}
                                        </label>
                                    </div>
                                @endforeach

                            </label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    Referencia
                </div>
                <div class="panel-body">
                    {!! Form::text('referencia',null,['class' => 'form-control', 'placeholder' => 'Referencia...', 'required']) !!}

                </div>
            </div>

            <div class="form-group">
                {!! Form::submit('Registrar',['class' => 'btn btn-primary'])!!}
            </div>

            {!! Form::close() !!}
        </div>
    </div>


@endsection
@section('js')
    <script>
        $('.select-padre').chosen({
            placeholder_text_single: "Selecciona una opción",
            no_results_text: "No se han encontrado "
        });
    </script>

    <script>
        /**
         * Cuando se seleccionen los productos, consultamos los proveedores que tienen todos los productos que se pondrán en la cotización
         */
        $("#select-marca").change(function () {
            crearCheckbox('select-producto', 'select-marca', 'label-materiales', 'materiales/');
        });
    </script>

    <script>
        /**
         * Función que crea checkbox y los habilita dependiendo de su estado
         **/
        function crearCheckbox(producto_id, marca_id, label_id, ruta) {

            /** Obtenemos los valores de las listas para comprobar los materiales que ya se han seleccionado **/
            var producto_id = document.getElementById(producto_id).value;
            var marca_id = document.getElementById(marca_id).value;

            // si no se ha seleccionado nada, entonces dejamos la lista vacia
            if (!producto_id || !marca_id) {
                $('#' + label_id).html('selecciona');
                return;
            }

            /** Obtenemos los valores de la ruta que recibimos por parametro **/
            $.get(ruta + producto_id + '/' + marca_id, function (data) {
                console.log(data);

                //variable que contiene los checkbox que se pondrán en materiales
                var html_select = '';

                for (var i = 0; i < data.length; i++) {
//                    console.log(data[i].nombre);
                    html_select += '<div class="checkbox">';
                    html_select += '<label>';
                    /** Comprobamos si el elemento está disponible para marcar **/
                    if (data[i].habilitado) {
                        html_select += '<input type="checkbox" name="almacen_materiales_id[]" value="' + data[i].id + '">';
                    }
                    else {
                        html_select += '<input type="checkbox" name="almacen_materiales_id[]" value="#" disabled>';
                    }
                    html_select += data[i].nombre;
                    html_select += '</label>';
                    html_select += '</div>';
                }

                $('#' + label_id).html(html_select);


            });
        }
    </script>
@endsection


