@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Productos Proveedor
@endsection

@section('main-content')
    <div class="panel panel-default">
        <div class="panel-heading">Productos por proveedor</div>
        <div class="panel-body">

            <a href="{{ route('almacen.configuracion.productosproveedor.create') }}" class="btn btn-info">
                Asignar un producto
            </a>

            <!-- BUSCADOR DE TAGS -->
            {!! Form::open(['route' => 'almacen.configuracion.productosproveedor.index', 'method' => 'GET', 'class' => 'navbar-form pull-right']) !!}
            <div class="input-group">
                {!! Form::text('buscar',null, ['class' => 'form-control', 'placeholder' => 'buscar producto...', 'aria-describedby' => 'search']) !!}
                <span class="input-group-addon" id="search">
                    <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                </span>
            </div>
            {!! Form::close() !!}
        <!-- FIN DEL BUSCADOR -->

            @if(count($productos)>0)
                <table class="table table-hover">
                    <thead>
                    <th>Producto</th>
                    <th>Marca</th>
                    <th>Material</th>
                    <th>Proveedor</th>
                    <th>Referencia</th>
                    <th>Codigo</th>
                    <th>Eliminado</th>
                    <th></th>
                    </thead>

                    <tbody>
                    @foreach($productos As $producto)
                        <tr>
                            <td>{{$producto->ProductoGeneral->Producto->nombre}}</td>
                            <td>{{$producto->ProductoGeneral->Marca->nombre}}</td>
                            <td>{{$producto->ProductoGeneral->Material->nombre}}</td>
                            <td>{{$producto->Proveedor->razon_social}}</td>
                            <td>{{$producto->referencia}}</td>
                            <td>{{$producto->codigo}}</td>
                            <td>{{$producto->eliminado ? "SI": "NO"}}</td>
                            <td>
                                <a href="{{route('almacen.configuracion.productosproveedor.edit',$producto->id)}}" class="btn btn-warning">
                                    <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                                </a>
                                <a href="{{route('almacen.configuracion.productosproveedor.destroy',$producto->id)}}"
                                   onclick=" return confirm('Desea eliminar este insumo?')" class="btn btn-danger">
                                <span class="glyphicon glyphicon-remove" aria-hidden="true">
                                </span>
                                </a>

                            </td>
                         </tr>
                    @endforeach
                    </tbody>
                </table>
                {!! $productos->render() !!}
            @else
                <hr>
                <p class="bg-warning text-center">
                    No se han registrado productos
                </p>
            @endif
        </div>
    </div>
@endsection


