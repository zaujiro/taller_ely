@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Orden Insumos
@endsection


@section('main-content')
    <div class="panel panel-default">
        <div class="panel-body">
            <a href="{{route('almacen.compras.insumos.index')}}" class="btn btn-default">
                Volver
            </a>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">Nueva orden</div>
        <div class="panel-body">
            <div class="form-group">
                {!! Form::open(['route' => 'almacen.compras.insumos.store', 'method' => 'POST','files' => true]) !!}

                <div class="panel panel-default">
                    <div class="panel-body">

                        <div class="form-group">
                            {!! Form::label('insumo_id','Insumos') !!}
                            {!! Form::select('insumo_id[]',$insumos,null,['class' => 'form-control select-padre', 'multiple','id'=>'select-insumo','placeholder' => '', 'required']) !!}
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        Precios
                    </div>

                    <ul class="list-group">
                        <div id = "formularioPrecios"> </div>
                    </ul>
                </div>

                <div class="form-group">
                    {!! Form::submit('Registrar',['class' => 'btn btn-primary'])!!}
                </div>

                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script>
        $('.select-padre').chosen({
            placeholder_text_single: "Selecciona una opción",
            no_results_text: "No se han encontrado ",
            disable_search_threshold: 0
        });

        //$('.select-padre').trigger('chosen:updated');
    </script>

    <!-- Llamado al script de las listas dependientes -->
    <script>
        /**
         * Cuando se seleccionen los productos, consultamos los proveedores que tienen todos los productos que se pondrán en la cotización
         */
        $("#select-insumo").change(function () {
            /** Creamos una variable que contiene los valores que están en las etiquetas <option>Valores</option>**/
            var textOptions = '';
            /** Obtenemos solos los valores seleccionados en la lista multiple y los separamos por un caracter**/
            $("#select-insumo option:selected").each(function () {
                textOptions += $(this).text() + "|";
            });

            crearCampos(textOptions);
        });
    </script>

    <script>
        /**
         * Función que toma los valores del select de productos y crea los campos correspondientes para los precios
         * de cada uno con el nombre de cada prodcuto ( producto - precio)
         */
        function crearCampos(textOptions) {
            /**Variable que contiene el formulario con los campos de producto - precio**/
            var formulario = '';

            /** Separamos la cadena de texto que contiene los nombres de los productos separados por un caracter "|" **/
            textOptions = textOptions.split('|');

            /** El arreglo de manera predeterminana estaba creando dos elementos vacios, uno al inicio y otro al final
             * con las siguientes funciones, se eliminan. Pop 1 elemento, Shift ultimo elemento**/
            textOptions.pop();
            textOptions.shift();

            /***
             * Leemos todos los valores seleccionados y creamos los campos de precio para cada producto
             */
            for (var i = 0; i < textOptions.length; i++) {
                formulario += '<li class="list-group-item">';
                formulario += '<div class="row">';
                formulario += '<div class="col-md-4">' + textOptions[i] + '</div>';
                formulario += '<div class="col-md-4">';
                formulario += '<input type="number" class="form-control" name="cantidad[]" min ="0" placeholder="Cantidad..." required>';
                formulario += '</div>';
                formulario += '<div class="col-md-4">';
                formulario += '<input type="number" class="form-control" name="precio[]" min ="0" placeholder="Precio..." required>';
                formulario += '</div>';
                formulario += '</div>';
                formulario += '</li>';
            }
            /** Cambiamos el valor del div con el id formularioPrecios por el formulario que hemos creado**/
            $('#formularioPrecios').html(formulario);
        }
    </script>

    <script>
        /**
         * Aplicamos el estilo al text area para ponerlo como un editor
         * con la libreria Trumbowyg
         * https://alex-d.github.io/Trumbowyg/
         */
        $('.textarea-content').trumbowyg();
    </script>
@endsection

