@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Aprobar Cotizaciones - Revisión de precio
@endsection


@section('main-content')
    <div class="panel panel-default">
        <div class="panel-heading">Aprobar Cotizaciones - Revisión de precio</div>
        <div class="panel-body">

            @if(count($cotizaciones)>0)
                <table class="table table-hover">
                    <thead>
                    <th>Estado</th>
                    <th>Cotización N°</th>
                    <th>Fecha de la cotización</th>
                    <th>Productos</th>
                    <th>Proveedor</th>
                    <th>Cotización</th>
                    <th></th>
                    </thead>

                    <tbody>
                    @foreach($cotizaciones As $cotizacion)

                        <tr>
                            <td>
                                @include('vendor.foods-online.compras.cotizaciones.confirmaciones')
                            </td>
                            <td>{{$cotizacion->id}}</td>
                            <td>{{$cotizacion->fecha_cotizacion}}</td>
                            <td>

                                <ul class="list-group">
                                    @foreach($cotizacion->Productos As $producto)
                                        <li>{{$producto->producto->nombre}}</li>
                                    @endforeach
                                </ul>
                            </td>
                            <td>{{$cotizacion->Proveedor->razon_social}}</td>
                            <td>
                                @if($cotizacion->cotizacion_pdf != '')
                                    <a href="{{asset('productos'.$cotizacion->cotizacion_pdf)}}"
                                       target="_blank">
                                        Cotización
                                    </a>
                                @else
                                    ---
                                @endif

                            </td>

                            <td>
                                <a href="#" class="btn btn-default" data-toggle="modal"
                                   data-target="#modalVerCotizacion{{$cotizacion->id}}">
                                        <span class="glyphicon glyphicon-eye-open" aria-hidden="true">
                                        </span>
                                </a>

                                <!-- Modal -->
                                <div class="modal fade" id="modalVerCotizacion{{$cotizacion->id}}" tabindex="-1"
                                     role="dialog"
                                     aria-labelledby="myModalLabel">
                                    <div class="modal-dialog modal-lg" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close"><span aria-hidden="true">&times;</span>
                                                </button>
                                                <h4 class="modal-title" id="myModalLabel">Datos de la cotización</h4>
                                            </div>
                                            <div class="modal-body">

                                                <!-- Incluimos los estados de la cotización -->
                                                @include('productos')

                                                <!-- Incluimos los datos de la cotización -->
                                                @include('vendor.foods-online.compras.cotizaciones.datos')

                                                {!! Form::open(['route' => ['compras.cotizaciones.aprobar',$cotizacion], 'method'=>'PUT']) !!}

                                                @php
                                                    /** Esta variable nos indicará si algún producto tiene análisis realizados **/
                                                        $resultados = false;
                                                @endphp

                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        Productos
                                                    </div>
                                                    <div class="panel-body">
                                                        <table class="table table-bordered">
                                                            <thead>
                                                            <tr class="info">
                                                                <th>
                                                                    <div class="text-center">
                                                                        {!! Form::checkbox('todos',1,false,['onclick'=>'marcar(this)']) !!}
                                                                    </div>
                                                                </th>
                                                                <th>Tipo</th>
                                                                <th>Producto</th>
                                                                <th>Precio</th>
                                                            </tr>
                                                            </thead>
                                                            @php
                                                                $botonEnable = true;
                                                            @endphp

                                                            <tbody>
                                                            @foreach($cotizacion->Productos As $producto)
                                                                <!-- Si este producto tiene análisis realizados, cambiamos el estado de la variable -->
                                                                @if($producto->AnalisisResultados->count())
                                                                    @php
                                                                        /** Cambiamos el estado de la variable resultados para mostrar los análisis**/
                                                                            $resultados = true;
                                                                    @endphp
                                                                @endif
                                                                <tr>
                                                                    <td>
                                                                        @if($producto->CotizacionConfirmacionProductoPrecio)

                                                                            @php
                                                                                $botonEnable = false;
                                                                                $confirmacion = $producto->CotizacionConfirmacionProductoPrecio;
                                                                            @endphp

                                                                            @if($confirmacion->estado == 1)
                                                                                <div class="text-center">
                                                                                    <span class="label label-success">
                                                                                        <span class="glyphicon glyphicon-ok-circle"
                                                                                              aria-hidden="true">
                                                                                        </span>
                                                                                    </span>
                                                                                </div>
                                                                            @elseif($confirmacion->estado == 2)
                                                                                <div class="text-center">
                                                                                    <span class="label label-danger">
                                                                                        <span class="glyphicon glyphicon-remove-sign"
                                                                                              aria-hidden="true">
                                                                                        </span>
                                                                                    </span>
                                                                                </div>
                                                                            @endif
                                                                        @else
                                                                            <div class="text-center">
                                                                                {!! Form::checkbox('compras_cotizaciones_productos_id[]',$producto->id,false) !!}
                                                                            </div>
                                                                        @endif
                                                                    </td>
                                                                    <td>{{$producto->TipoProducto->nombre}}</td>
                                                                    <td>{{$producto->producto->nombre}}</td>
                                                                    <td>${{$producto->precio}}</td>
                                                                </tr>
                                                            @endforeach

                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>


                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        Observaciones
                                                    </div>
                                                    <div class="panel-body">
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    @if($cotizacion->observaciones == null)
                                                                        -
                                                                    @else
                                                                        {!!$cotizacion->observaciones!!}
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <!-- Incluimos la vista que muestra los resultados de los análisis --->
                                                @include('vendor.foods-online.calidad.homologacion.cotizacion.resultados')

                                                @if($cotizacion->estado_final == null and $botonEnable)
                                                    <div class="text-center">
                                                        <div class="form-group">
                                                            {!! Form::submit('Aceptar',['class' => 'btn btn-success'])!!}
                                                        </div>
                                                    </div>
                                                @endif
                                            </div>

                                            {!! Form::close() !!}

                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">
                                                    Cancelar
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {!! $cotizaciones->render() !!}
            @else
                <hr>
                <p class="bg-warning text-center">
                    No se han registrado nuevas cotizaciones
                </p>
            @endif
        </div>
    </div>
@endsection
@section('js')
    <!-- Importamos el script para marcar todos los checkbox-->
    <script src="{{asset('js/marcarCheckbox.js')}}"></script>
@endsection



