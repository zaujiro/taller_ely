@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Actualizar proveedor
@endsection


@section('main-content')

    <div class="panel panel-default">
        <div class="panel-body">
            <a href="{{route('almacen.configuracion.proveedores.index')}}" class="btn btn-default">
                Volver
            </a>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">Actualizar proveedor</div>
        <div class="panel-body">
            <div class="form-group">
                {!! Form::open(['route' => ['almacen.configuracion.proveedores.update',$proveedor], 'method' => 'PUT']) !!}

                <div class="form-group">
                    {!! Form::label('razon_social','Razon Social') !!}
                    {!! Form::text('razon_social',$proveedor->razon_social,['class' => 'form-control',
                        'placeholder' => 'Razon social...', 'required']) !!}

                    {!! Form::label('telefono','Telefono') !!}
                    {!! Form::number('telefono',$proveedor->telefono,['class' => 'form-control',
                        'placeholder' => 'Telefono...', 'required']) !!}

                    {!! Form::label('celular','Celular') !!}
                    {!! Form::number('celular',$proveedor->celular,['class' => 'form-control',
                        'placeholder' => 'Celular...', 'required']) !!}

                    {!! Form::label('email','Correo') !!}
                    {!! Form::email('email',$proveedor->email,['class' => 'form-control',
                        'placeholder' => 'Correo...', 'required']) !!}

                    {!! Form::label('direccion','Direccion') !!}
                    {!! Form::text('direccion',$proveedor->direccion,['class' => 'form-control',
                        'placeholder' => 'Direccion...', 'required']) !!}
                </div>

                <div class="form-group">
                    <div class="panel panel-default">
                        <div class="panel-body">
                        {!! Form::label('productos_id','Tipos de productos: *') !!}
                        <!-- Acordeón -->
                            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                @foreach($productos As $producto)
                                    <div class="panel panel-default">
                                        <div class="panel-heading" role="tab" id="headingTwo">
                                            <h4 class="panel-title">
                                                <a class="collapsed" role="button" data-toggle="collapse"
                                                   data-parent="#accordion" href="#collapse{{$producto->id}}"
                                                   aria-expanded="false"
                                                   aria-controls="collapseTwo">
                                                    {{$producto->nombre}} ({{$producto->ProductosGeneral->count()}})
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapse{{$producto->id}}" class="panel-collapse collapse"
                                             role="tabpanel"
                                             aria-labelledby="headingTwo">
                                            <div class="panel-body">
                                                @foreach($producto->ProductosGeneral As $key => $productoGeneral)

                                                    @php
                                                        $checkProducto=false;

                                                        // Tomamos todos los productos del proveedor y buscamos el producto que actualamente estámos leyendo
                                                        $productoProveedor = $proveedor->Productos->where('almacen_productos_generales_id',$productoGeneral->id)->first();
                                                        // Valor inicial de la referencia
                                                        $referencia = null;
                                                        //Variable para marcar si el campo de referencia estará disponible
                                                        $disabled ="disabled";
                                                    @endphp

                                                    {{-- Si el proveedor tiene el producto, cambiamos el valor de la variable por true para marcar el checkbox
                                                       y habilitar la referencia para  poner el valor--}}
                                                    @if($productoProveedor)
                                                        @php
                                                            $checkProducto=true;
                                                            $referencia = $productoProveedor->referencia;
                                                            $disabled = null;
                                                        @endphp
                                                    @endif

                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="checkbox">
                                                                <label>
                                                                    {!! Form::checkbox('almacen_productos_generales_id[]', $productoGeneral->id,$checkProducto,['id','=',$productoGeneral->id,'onclick','=','habilitarCamposRango(this.id)']) !!}
                                                                    {{$productoGeneral->Marca->nombre}} -
                                                                    {{$productoGeneral->Material->nombre}}
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            {!! Form::text('referencia[]',$referencia,['class' => 'form-control','id','=','referencia'.$productoGeneral->id,'placeholder' => 'Referencia...',$disabled]) !!}
                                                        </div>
                                                    </div>
                                                @endforeach

                                            </div>
                                        </div>
                                    </div>
                                @endforeach

                            </div>
                            <!-- Acordeón -->
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::submit('Editar',['class' => 'btn btn-primary'])!!}
                </div>

                {!! Form::close() !!}
            </div>

        </div>
    </div>
@endsection

@section('js')
    <script>
        /**
         * Función que habilita/deshabilita el campo de referencia dependiendo si se ha marcado el checkbox del producto
         * @param idCheckbox
         */
        function habilitarCamposRango(idCheckbox) {
            document.getElementById(idCheckbox).onchange = function () {
                document.getElementById('referencia' + idCheckbox).disabled = !this.checked;
            };
        }
    </script>
@endsection