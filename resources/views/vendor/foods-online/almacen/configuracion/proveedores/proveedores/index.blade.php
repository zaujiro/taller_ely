@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Proveedores
@endsection


@section('main-content')
    <div class="panel panel-default">
        <div class="panel-heading">Proveedores</div>
        <div class="panel-body">

            <a href="{{ route('almacen.configuracion.proveedores.create') }}" class="btn btn-info">Registrar nuevo proveedor</a>

            <!-- BUSCADOR DE TAGS -->
            {!! Form::open(['route' => 'almacen.configuracion.proveedores.index', 'method' => 'GET', 'class' => 'navbar-form pull-right']) !!}
            <div class="input-group">
                {!! Form::text('buscar',null, ['class' => 'form-control', 'placeholder' => 'proveedores', 'aria-describedby' => 'search']) !!}
                <span class="input-group-addon" id="search">
                    <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                </span>
            </div>
            {!! Form::close() !!}
            <!-- FIN DEL BUSCADOR -->

            @if(count($proveedores)>0)
                <table class="table table-hover">
                    <thead>
                    <th>NIT</th>
                    <th>Razon  Social</th>
                    <th>Telefono</th>
                    <th>Celular</th>
                    <th>Correo</th>
                    <th>Direccion</th>
                    <th>Eliminado</th>
                    </thead>

                    <tbody>
                    @foreach($proveedores As $proveedor)
                    <tr>
                        <td>{{$proveedor->nit}}</td>
                        <td>{{$proveedor->razon_social}}</td>
                        <td>{{$proveedor->telefono}}</td>
                        <td>{{$proveedor->celular}}</td>
                        <td>{{$proveedor->email}}</td>
                        <td>{{$proveedor->direccion}}</td>
                        <td>{{$proveedor->eliminado ? "SI": "NO"}}</td>
                        <td>
                            <a href="{{route('almacen.configuracion.proveedores.edit',$proveedor->id)}}" class="btn btn-warning">
                                <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                            </a>
                            <a href="{{route('almacen.configuracion.proveedores.destroy',$proveedor->id)}}" onclick="
                                return confirm('Desea eliminar este Proveedor?')" class="btn btn-danger">
                                <span class="glyphicon glyphicon-remove" aria-hidden="true">
                                </span>
                            </a>

                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
                {!! $proveedores->render() !!}
                @else
                <hr>
                <p class="bg-warning text-center">
                    No se han registrado proveedores
                </p>
            @endif
        </div>
    </div>
@endsection


