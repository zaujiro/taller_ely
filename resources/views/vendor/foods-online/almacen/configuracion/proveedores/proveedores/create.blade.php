@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Nuevo proveedor
@endsection


@section('main-content')

    <div class="panel panel-default">
        <div class="panel-body">
            <a href="{{route('almacen.configuracion.proveedores.index')}}" class="btn btn-default">
                Volver
            </a>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">Crear proveedor</div>
        <div class="panel-body">
            <div class="form-group">
                {!! Form::open(['route' => 'almacen.configuracion.proveedores.store', 'method' => 'POST']) !!}

                <div class="form-group">
                    {!! Form::label('nit','NIT') !!}
                    {!! Form::text('nit',null,['class' => 'form-control', 'placeholder' => 'NIT...', 'required']) !!}

                    {!! Form::label('razon_social','Razon Social') !!}
                    {!! Form::text('razon_social',null,['class' => 'form-control', 'placeholder' => 'Razon social...', 'required']) !!}

                    {!! Form::label('telefono','Telefono') !!}
                    {!! Form::number('telefono',null,['class' => 'form-control', 'placeholder' => 'Telefono...', 'required']) !!}

                    {!! Form::label('celular','Celular') !!}
                    {!! Form::number('celular',null,['class' => 'form-control', 'placeholder' => 'Celular...', 'required']) !!}

                    {!! Form::label('email','Correo') !!}
                    {!! Form::email('email',null,['class' => 'form-control', 'placeholder' => 'Correo...', 'required']) !!}

                    {!! Form::label('direccion','Direccion') !!}
                    {!! Form::text('direccion',null,['class' => 'form-control', 'placeholder' => 'Direccion...', 'required']) !!}


                </div>

                <div class="form-group">
                    <div class="panel panel-default">
                        <div class="panel-body">
                        {!! Form::label('productos_id','Tipos de productos: *') !!}
                        <!-- Acordeón -->
                            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                @foreach($productos As $producto)
                                    <div class="panel panel-default">
                                        <div class="panel-heading" role="tab" id="headingTwo">
                                            <h4 class="panel-title">
                                                <a class="collapsed" role="button" data-toggle="collapse"
                                                   data-parent="#accordion" href="#collapse{{$producto->id}}"
                                                   aria-expanded="false"
                                                   aria-controls="collapseTwo">
                                                    {{$producto->nombre}} ({{$producto->ProductosGeneral->count()}})
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapse{{$producto->id}}" class="panel-collapse collapse"
                                             role="tabpanel"
                                             aria-labelledby="headingTwo">
                                            <div class="panel-body">
                                                @foreach($producto->ProductosGeneral As $key => $productoGeneral)
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="checkbox">
                                                                <label>
                                                                    {!! Form::checkbox('almacen_productos_generales_id[]', $productoGeneral->id,null,['id','=',$productoGeneral->id,'onclick','=','habilitarCamposRango(this.id)']) !!}
                                                                    {{$productoGeneral->Marca->nombre}} -
                                                                    {{$productoGeneral->Material->nombre}}
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            {!! Form::text('referencia[]',null,['class' => 'form-control','id','=','referencia'.$productoGeneral->id,'placeholder' => 'Referencia...','disabled']) !!}
                                                        </div>
                                                    </div>
                                                @endforeach

                                            </div>
                                        </div>
                                    </div>
                                @endforeach

                            </div>
                            <!-- Acordeón -->
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::submit('Registrar',['class' => 'btn btn-primary'])!!}
                </div>

                {!! Form::close() !!}
            </div>

        </div>
    </div>
@endsection

@section('js')
    <script>
        /**
         * Función que habilita/deshabilita el campo de referencia dependiendo si se ha marcado el checkbox del producto
         * @param idCheckbox
         */
        function habilitarCamposRango(idCheckbox) {
            document.getElementById(idCheckbox).onchange = function () {
                document.getElementById('referencia' + idCheckbox).disabled = !this.checked;
            };
        }
    </script>
@endsection


