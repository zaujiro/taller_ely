@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Compra de insumos
@endsection

@section('main-content')
    <div class="panel panel-default">
        <div class="panel-heading">Compra de insumos</div>
        <div class="panel-body">

            <a href="{{ route('almacen.compras.insumos.create') }}" class="btn btn-info">
                Registrar nueva orden
            </a>

            <!-- BUSCADOR DE TAGS -->
            {!! Form::open(['route' => 'almacen.compras.insumos.index', 'method' => 'GET', 'class' => 'navbar-form pull-right']) !!}
            <div class="input-group">
                {!! Form::text('buscar',null, ['class' => 'form-control', 'placeholder' => 'buscar orden...', 'aria-describedby' => 'search']) !!}
                <span class="input-group-addon" id="search">
                    <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                </span>
            </div>
            {!! Form::close() !!}
        <!-- FIN DEL BUSCADOR -->

            @if(count($ordenes)>0)
                <table class="table table-hover">
                    <thead>
                    <th>N° Orden de compra</th>
                    <th>Fecha de orden</th>
                    <th>Usuario</th>
                    <th></th>
                    </thead>

                    <tbody>
                    @foreach($ordenes As $orden)
                        <tr>
                            <td>{{$orden->id}}</td>
                            <td>{{$orden->created_at->format('Y-m-d')}}</td>
                            <td>{{$orden->User->name}}</td>
                            <td>
                                <a href="#" class="btn btn-default" data-toggle="modal"
                                   data-target="#modalVerCotizacion{{$orden->id}}">
                                        <span class="glyphicon glyphicon-eye-open" aria-hidden="true">
                                        </span>
                                </a>

                                <!-- Modal -->
                                <div class="modal fade" id="modalVerCotizacion{{$orden->id}}" tabindex="-1"
                                     role="dialog"
                                     aria-labelledby="myModalLabel">
                                    <div class="modal-dialog modal-lg" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close"><span aria-hidden="true">&times;</span>
                                                </button>
                                                <h4 class="modal-title" id="myModalLabel">Datos de la orden</h4>
                                            </div>
                                            <div class="modal-body">
                                                <!-- Incluimos los datos de la cotización -->
                                                @include('vendor.foods-online.almacen.compras.insumos.datos')

                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        Insumos
                                                    </div>
                                                    <div class="panel-body">
                                                        <table class="table table-bordered">
                                                            <tbody>

                                                            <thead>
                                                            <th>Nombre</th>
                                                            <th>Cantidad</th>
                                                            <th>Precio</th>
                                                            </thead>
                                                            <!-- Leemos todos los productos de la cotización -->
                                                            @foreach($orden->Insumos As $insumo)
                                                                <!-- Si este producto tiene análisis realizados, cambiamos el estado de la variable -->
                                                                <tr>
                                                                    <td>{{$insumo->Insumo->nombre}}</td>
                                                                    <td>{{$insumo->cantidad}}</td>
                                                                    <td>${{$insumo->precio}}</td>
                                                                </tr>
                                                            @endforeach

                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div><!-- Close modal-body -->
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">
                                                    Ok
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <a href="{{route('almacen.compras.insumos.destroy',$orden->id)}}" onclick=" return confirm('Desea eliminar este insumo?')" class="btn btn-danger">
                                <span class="glyphicon glyphicon-remove" aria-hidden="true">
                                </span>
                                </a>
                            </td>
                         </tr>
                    @endforeach
                    </tbody>
                </table>
                {!! $ordenes->render() !!}
            @else
                <hr>
                <p class="bg-warning text-center">
                    No se han registrado ordenes
                </p>
            @endif
        </div>
    </div>
@endsection


