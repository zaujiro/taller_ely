<!-- Datos de la cotización -->
<div class="panel panel-default">
    <div class="panel-heading">
        Datos de la orden
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-2 col-lg-4 text-center">
                <small>N° Orden de compra</small>
                <h4>{{$orden->id}}</h4>
            </div>

            <div class="col-sm-2 col-lg-4 text-center">
                <small>Fecha de orden</small>
                <h4>{{$orden->created_at->format('Y-m-d')}}</h4>
            </div>

            <div class="col-sm-2 col-lg-4 text-center">
                <small>Usuario</small>
                <h4>{{$orden->User->name}}</h4>
            </div>


        </div>
    </div>
</div>