@php
    $empresa = $empresa->first();
@endphp
<!-- Datos de la empresa -->

<div class="form-group">
    <div class="row">
        <div class="col-xs-2 col-lg-5">
            <img src="{{asset('img/foods-online/empresa/'.$empresa->logo)}}" class="img-responsive"
                 alt="...">
        </div>

        <div class="col-xs-2 text-left">
            <div class="clearfix">
                <b>NIT :</b>
            </div>
            <div class="clearfix">
                <b>Nombre :</b>
            </div>
            <div class="clearfix">
                <b>Teléfono(s) :</b>
            </div>
            <div class="clearfix">
                <b>Celular(es) :</b>
            </div>
        </div>

        <div class="col-xs-4 text-left">
            <div class="clearfix">
                {{$empresa->nit}}
            </div>

            <div class="clearfix">
                {{$empresa->nombre}}
            </div>
            <div class="clearfix">
                {{$empresa->telefono}}
            </div>
            <div class="clearfix">
                {{$empresa->celular}}
            </div>
            <div class="clearfix">
                {{$empresa->municipio->departamento->nombre}} -
                {{$empresa->municipio->nombre}}
            </div>
        </div>
    </div>
</div>
