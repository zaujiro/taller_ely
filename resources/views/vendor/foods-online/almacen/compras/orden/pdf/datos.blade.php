<!-- Datos de la cotización -->
<div class="panel panel-primary">
    <div class="panel-heading">
        Datos de la orden de compra
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-xs-2 text-center">
                <small><u>Nº Orden</u></small>
                <h4>{{$orden->id}}</h4>
            </div>

            <div class="col-xs-2 text-center">
                <small><u>Fecha</u></small>
                <h4>{{$orden->fecha_orden}}</h4>
            </div>

            <div class="col-xs-4 text-center">
                <small><u>Solicitante</u></small>
                <h4>{{$orden->User->name}}</h4>
            </div>

            <div class="col-xs-2 text-center">
                <small><u>Crédito</u></small>
                <h4>{{$orden->credito ? "SI" : "NO"}}</h4>
            </div>
        </div>

    </div>
</div>