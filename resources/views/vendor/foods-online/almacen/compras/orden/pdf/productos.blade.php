<table class="table table-bordered">
    <thead>
    <tr class="info">
        <th class="text-center">Referencia</th>
        <th>Producto</th>
        <th>Marca</th>
        <th>Material</th>
        <th class="text-center">Cantidad solicitada</th>
    </tr>
    </thead>

    <tbody>
    <!-- Leemos todos los productos de la cotización -->
    @foreach($orden->Productos As $producto)
        <tr>
            <td class="text-center">{{$producto->referencia ? $producto->referencia: "-"}}</td>
            <td>{{$producto->ProductoProveedor->ProductoGeneral->Producto->nombre}}</td>
            <td>{{$producto->ProductoProveedor->ProductoGeneral->Marca->nombre}}</td>
            <td>{{$producto->ProductoProveedor->ProductoGeneral->Material->nombre}}</td>
            <td class="text-center">{{$producto->cantidad_solicitada}} </td>
        </tr>
    @endforeach

    </tbody>
</table>