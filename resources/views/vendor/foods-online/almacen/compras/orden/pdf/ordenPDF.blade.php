@php
    $orden = $orden->first();
@endphp
<html lang="es">
<head>
    <meta charset="UTF-8">
    {{--<title>Orden de compra #{{$orden->id}}</title>--}}
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <link href="{{ asset('/css/allPDF.css') }}" rel="stylesheet" type="text/css"/>
</head>


<body>
<!-- Incluimos la información de la empresa -->
@include('vendor.foods-online.almacen.compras.orden.pdf.empresa')

<!-- Incluimos los datos de la cotización -->
@include('vendor.foods-online.almacen.compras.orden.pdf.datos')

<!-- Incluimos los datos del proveedor -->
@include('vendor.foods-online.almacen.compras.orden.pdf.proveedor')


<div class="panel panel-primary">
    <div class="panel-heading">
        Productos
    </div>
    <div class="panel-body">
        <!-- Incluimos los datos de los productos -->
        @include('vendor.foods-online.almacen.compras.orden.pdf.productos')
    </div>
</div>

<div class="panel panel-primary">
    <div class="panel-heading">
        Observaciones
    </div>
    <div class="panel-body">
        <div class="form-group">
            <div class="row">
                <div class="col-md-12">
                    @if($orden->observaciones == null)
                        -
                    @else
                        {!!$orden->observaciones!!}
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>
