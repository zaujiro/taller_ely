<!-- Datos de la cotización -->
<div class="panel panel-primary">
    <div class="panel-heading">
        Datos del proveedor
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-xs-3 text-center">
                <small>
                    <u>NIT</u>
                </small>
                <h4>{{$orden->Proveedor->nit}}</h4>
            </div>

            <div class="col-xs-4 text-center">
                <small>
                    <u>Razón social</u>
                </small>
                <h4>{{$orden->Proveedor->razon_social}}</h4>
            </div>

            <div class="col-xs-4 text-center">
                <small>
                    <u>Teléfono</u>
                </small>
                <h4>{{$orden->Proveedor->telefono}}</h4>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-3 text-center">
                <small>
                    <u>Email</u>
                </small>
                <h4>{{$orden->Proveedor->email}}</h4>
            </div>

            <div class="col-xs-4 text-center">
                <small>
                    <u>Dirección</u>
                </small>
                <h4>{{$orden->Proveedor->direccion}}</h4>
            </div>

            <div class="col-xs-4 text-center">
                <small>
                    <u>Celular</u>
                </small>
                <h4>{{$orden->Proveedor->celular}}</h4>
            </div>

        </div>
    </div>
</div>