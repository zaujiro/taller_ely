@if($orden->estado == null)
    <span class="label label-default">En proceso</span>
@elseif($orden->estado == 1)
    <span class="label label-success">Recibida</span>
@elseif($orden->estado == 2)
    <span class="label label-warning">Anulada</span>
@endif