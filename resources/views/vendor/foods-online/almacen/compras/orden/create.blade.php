@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Ordenes de compra
@endsection

@section('main-content')
    <div class="panel panel-default">
        <div class="panel-body">
            <a href="{{route('compras.ordenes.index')}}" class="btn btn-default">
                Volver
            </a>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">Nueva orden de compra</div>
        <div class="panel-body">
            <div class="form-group">
                {!! Form::open(['route' => 'compras.ordenes.store', 'method' => 'POST','files' => true]) !!}

                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="form-group">
                            {!! Form::label('fecha_orden','Fecha de la orden de compra') !!}
                            {!! Form::date('fecha_orden',\Carbon\Carbon::now(),['class' => 'form-control', 'required']) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('almacen_proveedores_id','Proveedor') !!}
                            {!! Form::select('almacen_proveedores_id',$proveedores,null,['class' => 'form-control select-padre', 'id'=>'select-proveedor','placeholder' => 'seleccione una opción', 'required']) !!}
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        Precios
                    </div>

                    <ul class="list-group">
                        <!-- Div donde se pondrán los campos de los productos con su respetiva cantidad a solicitar-->
                        <div id="formularioPrecios"></div>
                    </ul>

                </div>

                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="form-group">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('credito',1) !!}
                                    Solicitado a Crédito
                                </label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('observaciones','Observaciones') !!}
                    {!! Form::textarea('observaciones',null,['class' => 'form-control textarea-content']) !!}
                </div>

                <div class="form-group">
                    {!! Form::submit('Registrar',['class' => 'btn btn-primary'])!!}
                </div>

                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
@section('js')
    <!-- Importamos el script que crea los campos para cantidad de cada producto -->
    <script src="{{asset('js/foods-online/compras/ordenes/crearCampos.js')}}"></script>
    <!-- Importamos el script que realiza la operación por producto de precio*cantidad  -->
    <script src="{{asset('js/foods-online/compras/ordenes/cambiarTotal.js')}}"></script>

    <script>
        $('.select-padre').chosen({
            placeholder_text_single: "Selecciona una opción",
            no_results_text: "No se han encontrado ",
            disable_search_threshold: 0
        });
    </script>

    <!-- Llamado al script de las listas dependientes -->
    <script>

        /**
         * Cuando cambie la lista de proveedor, traemos todos los productos disponibles de este proveedor
         */
        $("#select-proveedor").change(function () {

            /** Obtenemos el id del proveedor **/
            var proveedorId = $(this).val();

            var idSelectPadre = 'select-proveedor';
            var idSelectHijo = 'select-producto';
            var ruta = 'productos/';


            /** Borramos todo el contenido del div donde se crean los campos de los produtos**/
            $("#formularioPrecios").html("");

            //Función AJAX que cambia los valores de la lista dinamicamente
            $.get(ruta + proveedorId, function (data) {

                formulario = '';
                formulario += '<table class="table table-bordered">';
                formulario += '<thead>';
                formulario += '<th class="text-center">#</th>';
                formulario += '<th class="text-center">Referencia</th>';
                formulario += '<th>Producto</th>';
                formulario += '<th>Marca</th>';
                formulario += '<th>Material</th>';
                formulario += '<th>Cantidad</th>';
                formulario += '</thead>';
                formulario += '<tbody>';

                for (var i = 0; i < data.length; i++) {

                    formulario +='<input type="hidden" name="almacen_productos_proveedor_id[]" value="'+data[i].id+'">';
                    formulario += '<tr>';
                    formulario += '<td class="text-center">' + (i + 1) + '</td>';
                    formulario += '<td class="text-center">' + data[i].referencia + '</td>';
                    formulario += '<td>' + data[i].producto + '</td>';
                    formulario += '<td>' + data[i].marca + '</td>';
                    formulario += '<td>' + data[i].material + '</td>';
                    formulario += '<td>';
                    formulario += '<div class="row">';
                    formulario += '<div class="col-xs-5">';
                    formulario += '<input type="text" class="form-control" name="cantidad[]" min ="0" placeholder="cantidad...">';
                    formulario += '</div>';
                    formulario += '</div>';
                    formulario += '</td>';

                    formulario += '</tr>';

                }

                formulario += '</tbody>';
                formulario += '</table>';

                /** Cambiamos el valor del div con el id formularioPrecios por el formulario que hemos creado**/
                $('#formularioPrecios').html(formulario);
            });


        });
    </script>

@endsection

