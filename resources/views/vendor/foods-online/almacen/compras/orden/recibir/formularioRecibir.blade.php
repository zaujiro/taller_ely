{!! Form::open(['route' => ['compras.ordenes.recibir.save',$orden->id], 'method'=>'PUT']) !!}

<table class="table table-bordered">
    <thead>
    <tr class="info">
        <th class="text-center">Referencia</th>
        <th>Producto</th>
        <th>Marca</th>
        <th>Material</th>
        <th class="text-center">Cantidad ( recibida / solicitada )</th>
        <th>Bodega destino</th>
    </tr>
    </thead>

    <tbody>
    <!-- Leemos todos los productos de la cotización -->
    @foreach($orden->Productos As $key => $producto)
        {!! Form::hidden('id['.$key.']',$producto->id,null) !!}
        {!! Form::hidden('almacen_productos_generales_id['.$key.']',$producto->ProductoProveedor->ProductoGeneral->id,null) !!}
        <tr>
            <td class="text-center">{{$producto->referencia ? $producto->referencia: "-"}}</td>
            <td>{{$producto->ProductoProveedor->ProductoGeneral->Producto->nombre}}</td>
            <td>{{$producto->ProductoProveedor->ProductoGeneral->Marca->nombre}}</td>
            <td>{{$producto->ProductoProveedor->ProductoGeneral->Material->nombre}}</td>
            <td class="text-center">

                <div class="row">
                    <div class="col-xs-5">
                        {!! Form::number('cantidad['.$key.']',null,
                        ['class' => 'form-control','placeholder'=>'Cantidad...', 'min'=>'0','max'=>$producto->cantidad_solicitada,'required']) !!}
                    </div>
                    <div class="col-xs-1 text-center">
                        /
                    </div>
                    <div class="col-xs-3">
                        {{$producto->cantidad_solicitada}}
                    </div>
                </div>
            </td>
            <td>
                {!! Form::select('almacen_bodegas_id[]',$bodegas,null,['class' => 'form-control select-padre','placeholder' => '','required']) !!}
            </td>
        </tr>
    @endforeach

    </tbody>
</table>

<div class="form-group text-center">
    {!! Form::submit('Recibir',['class' => 'btn btn-primary'])!!}
</div>

{!! Form::close() !!}