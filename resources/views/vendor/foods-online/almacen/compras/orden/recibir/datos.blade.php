@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Recibir orden de compra
@endsection


@section('main-content')
    <div class="panel panel-default">
        <div class="panel-body">
            <a href="{{route('compras.ordenes.recibir.index')}}" class="btn btn-default">
                Volver
            </a>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="modal-title" id="myModalLabel">
                Datos de la orden de compra
            </h4>
        </div>
        <div class="panel-body">
        @if($orden)
            <!-- Incluimos los estados de la cotización -->
            @include('vendor.foods-online.almacen.compras.orden.estados')

            <!-- Incluimos los datos de la cotización -->
                @include('vendor.foods-online.almacen.compras.orden.datos')


                <div class="panel panel-default">
                    <div class="panel-heading">
                        Productos
                    </div>
                    <div class="panel-body">
                        @include('vendor.foods-online.almacen.compras.orden.recibir.formularioRecibir')
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        Observaciones
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    @if($orden->observaciones == null)
                                        -
                                    @else
                                        {!!$orden->observaciones!!}
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            @else
                <hr>
                <p class="bg-warning text-center">
                    No se ha encontrado la orden
                </p>
            @endif
        </div>
    </div>
@endsection


