@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Recibir ordenes de compra
@endsection


@section('main-content')
    <div class="panel panel-default">
        <div class="panel-heading">Recibir ordenes de compra</div>
        <div class="panel-body">
            <!-- BUSCADOR DE TAGS -->
            {!! Form::open(['route' => 'compras.ordenes.index', 'method' => 'GET', 'class' => 'navbar-form pull-right']) !!}
            <div class="input-group">
                {!! Form::text('buscar',null, ['class' => 'form-control', 'placeholder' => 'buscar orden...', 'aria-describedby' => 'search']) !!}
                <span class="input-group-addon" id="search">
                    <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                </span>
            </div>
            {!! Form::close() !!}
        <!-- FIN DEL BUSCADOR -->

            @if(count($ordenes)>0)
                <table class="table table-hover">
                    <thead>
                    <th>Estado</th>
                    <th>Orden N°</th>
                    <th>Fecha de la orden</th>
                    <th>Proveedor</th>
                    <th>Solicitante</th>
                    <th>Crédito</th>
                    <th></th>
                    </thead>

                    <tbody>
                    @foreach($ordenes As $orden)

                        <tr>
                            <td>
                                @include('vendor.foods-online.almacen.compras.orden.confirmaciones')
                            </td>
                            <td>{{$orden->id}}</td>
                            <td>{{$orden->fecha_orden}}</td>
                            <td>{{$orden->Proveedor->razon_social}}</td>
                            <td>{{$orden->User->name}}</td>
                            <td>{{$orden->credito ? "SI":"NO"}}</td>

                            <td>
                                <a href="#" class="btn btn-default" data-toggle="modal"
                                   data-target="#modalVerCotizacion{{$orden->id}}">
                                        <span class="glyphicon glyphicon-eye-open" aria-hidden="true">
                                        </span>
                                </a>

                                <a href="{{route('compras.ordenes.recibir.formulario',$orden->id)}}"
                                   class="btn btn-warning" title="Recibir orden de compra">
                                    <span class="glyphicon glyphicon-copy" aria-hidden="true"></span>
                                </a>

                                <!-- Modal -->
                                <div class="modal fade" id="modalVerCotizacion{{$orden->id}}" tabindex="-1"
                                     role="dialog"
                                     aria-labelledby="myModalLabel">
                                    <div class="modal-dialog modal-lg" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close"><span aria-hidden="true">&times;</span>
                                                </button>
                                                <h4 class="modal-title" id="myModalLabel">
                                                    Datos de la orden de compra
                                                </h4>
                                            </div>
                                            <div class="modal-body">
                                                <!-- Incluimos los estados de la cotización -->
                                            @include('vendor.foods-online.almacen.compras.orden.estados')

                                            <!-- Incluimos los datos de la cotización -->
                                                @include('vendor.foods-online.almacen.compras.orden.datos')

                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        Productos
                                                    </div>
                                                    <div class="panel-body">
                                                        <!-- Incluimos los datos de los productos -->
                                                        @include('vendor.foods-online.almacen.compras.orden.productos')
                                                    </div>
                                                </div>


                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        Observaciones
                                                    </div>
                                                    <div class="panel-body">
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    @if($orden->observaciones == null)
                                                                        -
                                                                    @else
                                                                        {!!$orden->observaciones!!}
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div><!-- Close modal-body -->

                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">
                                                    Ok
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {!! $ordenes->render() !!}
            @else
                <hr>
                <p class="bg-warning text-center">
                    No se han registrado ordenes de compra
                </p>
            @endif
        </div>
    </div>
@endsection


