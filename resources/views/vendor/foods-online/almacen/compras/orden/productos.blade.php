<table class="table table-bordered">
    <thead>
    <tr class="info">
        <th class="text-center">Referencia</th>
        <th>Producto</th>
        <th>Marca</th>
        <th>Material</th>
        <th class="text-center">Cantidad ( recibida / solicitada )</th>
    </tr>
    </thead>

    <tbody>
    <!-- Leemos todos los productos de la cotización -->
    @foreach($orden->Productos As $producto)
        <tr>
            <td class="text-center">{{$producto->referencia ? $producto->referencia: "-"}}</td>
            <td>{{$producto->ProductoProveedor->ProductoGeneral->Producto->nombre}}</td>
            <td>{{$producto->ProductoProveedor->ProductoGeneral->Marca->nombre}}</td>
            <td>{{$producto->ProductoProveedor->ProductoGeneral->Material->nombre}}</td>
            <td class="text-center">
                <label class="text-bold">{{$producto->cantidad_recibida}}</label>
                <label class="text-bold">/</label>
                <label>{{$producto->cantidad_solicitada}}</label>

            </td>
        </tr>
    @endforeach

    </tbody>
</table>