<!-- Estados de la cotización -->
<div class="panel panel-default">
    <div class="panel-heading">
        Confirmaciones
    </div>
    <div class="panel-body">
        <div class="form-group">
            <div class="row">
                <div class="col-xs-4 text-center">
                    <small>Estado</small>
                    <div class="text-center">
                        @include('vendor.foods-online.almacen.compras.orden.confirmaciones')
                    </div>
                </div>

                <div class="col-xs-4 text-center">
                    <small>Responsable</small>
                    <div class="text-center">
                        <div class="text-center">
                            {{$orden->User->name}}
                        </div>
                    </div>
                </div>

                <div class="col-xs-4 text-center">
                    <small>Rol</small>
                    <div class="text-center">
                        <div class="text-center">
                            <small>{{$orden->User->Rol->nombre}}</small>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>