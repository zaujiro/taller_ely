<!-- Datos de la cotización -->
<div class="panel panel-default">
    <div class="panel-heading">
        Datos de la orden de compra
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-2 col-lg-4 text-center">
                <small>Fecha de la orden de compra</small>
                <h4>{{$orden->fecha_orden}}</h4>
            </div>

            <div class="col-sm-2 col-lg-4 text-center">
                <small>Proveedor</small>
                <h4>{{$orden->Proveedor->razon_social}}</h4>
            </div>

            <div class="col-sm-2 col-lg-4 text-center">
                <small>Solicitante</small>
                <h4>{{$orden->User->name}}</h4>
            </div>
        </div>

    </div>
</div>