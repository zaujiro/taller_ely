@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Menú
@endsection


@section('main-content')
    <div class="panel panel-default">
        <div class="panel-heading">Menús</div>
        <div class="panel-body">

            <a href="{{ route('menu.create') }}" class="btn btn-info">Registrar nuevo menú</a>

            <!-- BUSCADOR DE TAGS -->
            {!! Form::open(['route' => 'menu.index', 'method' => 'GET', 'class' => 'navbar-form pull-right']) !!}
            <div class="input-group">
                {!! Form::text('buscar',null, ['class' => 'form-control', 'placeholder' => 'buscar menú...', 'aria-describedby' => 'search']) !!}
                <span class="input-group-addon" id="search">
                    <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                </span>
            </div>
            {!! Form::close() !!}
            <!-- FIN DEL BUSCADOR -->

            @if(count($menus)>0)
                <table class="table table-hover">
                    <thead>
                    <th>ID</th>
                    <th>Nombre</th>
                    <th>Ruta</th>
                    <th>Orden</th>
                    <th>Padre</th>
                    <th></th>
                    </thead>

                    <tbody>
                    @foreach($menus As $menu)
                    <tr>
                        <td>{{$menu->id}}</td>
                        <td>{{$menu->nombre}}</td>
                        <td>{{$menu->src}}</td>
                        <td>{{$menu->orden}}</td>
                        <td>{{$menu->id_padre}}</td>
                        <td>
                            <a href="{{route('menu.edit',$menu->id)}}" class="btn btn-warning">
                                <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                            </a>
                            <a href="{{route('menu.destroy',$menu->id)}}" onclick=" return confirm('Desea eliminar este menú?')" class="btn btn-danger">
                                <span class="glyphicon glyphicon-remove" aria-hidden="true">
                                </span>
                            </a>

                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
                @else
                <hr>
                <p class="bg-warning text-center">
                    No se han registrado menús
                </p>
            @endif
            {!! $menus->render() !!}
        </div>
    </div>
@endsection


