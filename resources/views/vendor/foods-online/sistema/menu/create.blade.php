@extends('adminlte::layouts.app')

@section('htmlheader_title')
    {{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')

    <div class="panel panel-default">
        <div class="panel-body">
            <a href="{{route('menu.index')}}" class="btn btn-default">
                Volver
            </a>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">Crear menú</div>
        <div class="panel-body">
            <div class="form-group">
                {!! Form::open(['route' => 'menu.store', 'method' => 'POST']) !!}


                <div class="form-group">
                    {!! Form::label('id_padre','Padre') !!}
                    {!! Form::select('id_padre',$menus,null,['class' => 'form-control select-padre','id'=>'select-id-padre', 'placeholder' => 'seleccione una opción', 'required']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('nombre','Nombre') !!}
                    {!! Form::text('nombre',null,['class' => 'form-control', 'placeholder' => 'Nombre del menú', 'required']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('src','Ruta') !!}
                    {!! Form::text('src',null,['class' => 'form-control', 'placeholder' => 'Ruta', 'required']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('orden','Orden') !!}
                    {!! Form::text('orden',null,['class' => 'form-control', 'placeholder' => 'Orden', 'required']) !!}
                    Orden máximo generado: <label id="label-max-orden"> - </label>
                </div>

                <div class="form-group">
                    {!! Form::label('icon','Icon') !!}
                    {!! Form::text('icon',null,['class' => 'form-control', 'placeholder' => 'icon']) !!}

                    Para más iconos visitar:
                    {!! link_to('http://fontawesome.io/icons/#web-application','fontawesome',['target'=>'_blank'],'null') !!}
                </div>

                <div class="form-group">
                    {!! Form::submit('Registrar',['class' => 'btn btn-primary'])!!}
                </div>

                {!! Form::close() !!}
            </div>

        </div>
    </div>
@endsection
@section('js')
    <script>
        $('.select-padre').chosen({
            placeholder_text_single: "Selecciona una opción",
            no_results_text: "No se han encontrado "
        });
    </script>

    <script>

        $("#select-id-padre").change(function () {
            OnselectMaxOrden('select-id-padre', 'label-max-orden', 'hijos/')
        });

        // Función para poner el maximo orden de un padre
        function OnselectMaxOrden(idSelectPadre, labelHijo, ruta) {

            var selectPadre = document.getElementById(idSelectPadre).value;

            // si no se ha seleccionado nada, entonces dejamos el label vacio
            if (!selectPadre) {
                $('#' + labelHijo).html('-');
                return;
            }

            //Función AJAX que cambia los valores de la lista dinamicamente
            $.get(ruta + selectPadre, function (data) {

                //variable que contiene el html que se pondrá en el label
                var html_select = data;

                if (data == '') {
                    html_select = '0';
                }

                // cambiamos el html del label
                $('#' + labelHijo).html(html_select)
            });
        }
    </script>
@endsection

