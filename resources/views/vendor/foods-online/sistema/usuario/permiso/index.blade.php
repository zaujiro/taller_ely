@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Permisos por rol
@endsection


@section('main-content')
    <div class="panel panel-default">
        <div class="panel-heading">Permisos de roles</div>
        <div class="panel-body">

            <a href="{{ route('permiso.create') }}" class="btn btn-info">Registrar nuevo permiso</a>

            <!-- BUSCADOR  -->
            {!! Form::open(['route' => 'permiso.index', 'method' => 'GET', 'class' => 'navbar-form pull-right']) !!}
            <div class="input-group">
                {!! Form::text('buscar',null, ['class' => 'form-control', 'placeholder' => 'busccar rol...', 'aria-describedby' => 'search']) !!}
                <span class="input-group-addon" id="search">
                    <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                </span>
            </div>
            {!! Form::close() !!}
            <!-- FIN DEL BUSCADOR -->


            @if(count($permisos)>0)
                <table class="table table-stripped">
                    <thead>
                    <th>#</th>
                    <th>Rol</th>
                    <th>N° enlaces</th>
                    <th>Habilitado</th>
                    </thead>

                    <tbody>
                    @php
                        $contador=1;
                    @endphp

                    @foreach($permisos As $key => $permiso)
                        <tr>
                            <td>{{$contador++ }}</td>
                            <td>{{$permiso->rol->nombre}}</td>
                            <td>{{$numeroEnlaces[$key]}}</td>
                            <td>{{$permiso->rol->habilitado}}</td>
                            <td>
                                <a href="{{route('permiso.edit',$permiso->ID_rol)}}" class="btn btn-success">
                                    <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @else
                <hr>
                <p class="bg-warning text-center">
                    No se han registrado permisos
                </p>
            @endif
            {!! $permisos->render() !!}
        </div>
    </div>
@endsection


