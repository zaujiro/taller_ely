@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Movimiento en inventario
@endsection


@section('main-content')
    {!! Form::open(['route' => 'inventario.movimiento.save', 'method' => 'PUT','files' => true]) !!}

    <div class="panel panel-default">
        <div class="panel-heading">Movimiento en inventario</div>
        <div class="panel-body">
            <div class="form-group">
                {{--{!! Form::open(['route' => ['inventario.configuracion.bodegas.update',$bodega], 'method'=>'PUT']) !!}--}}
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="form-group">
                            {!! Form::label('almacen_bodegas_id','*Bodega') !!}
                            {!! Form::select('almacen_bodegas_id',$bodegas,null,['class' => 'form-control select-padre','placeholder' => '']) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('almacen_productos_generales_id','*Producto ') !!}
                            {!! Form::select('almacen_productos_generales_id',$productos,null,['class' => 'form-control select-padre','placeholder' => '']) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('cantidad','*Cantidad ') !!}
                            {!! Form::number('cantidad',null,['class' => 'form-control','placeholder'=>'Cantidad...', 'min'=>'0','required']) !!}
                        </div>

                    </div>
                </div>

                <div class="form-group">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            {!! Form::label('tipo_movimiento','Tipo de movimiento: *') !!}
                                <div class="radio">
                                    <label>
                                        {!! Form::radio('tipo_movimiento', 1,null,['required']); !!}
                                        Entrada
                                    </label>
                                </div>
                            <div class="radio">
                                    <label>
                                        {!! Form::radio('tipo_movimiento', 2,null,['required']); !!}
                                        Salida
                                    </label>
                                </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::submit('Guardar',['class' => 'btn btn-primary'])!!}
                </div>

                {{--{!! Form::close() !!}--}}
            </div>
        </div>
    </div>

    {!! Form::close() !!}
@endsection
@section('js')
    <script>
        $('.select-padre').chosen({
            placeholder_text_single: "Selecciona una opción",
            no_results_text: "No se han encontrado "
        });
    </script>
@endsection
