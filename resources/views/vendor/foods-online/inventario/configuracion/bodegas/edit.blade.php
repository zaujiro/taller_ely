@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Actualizar bodega
@endsection


@section('main-content')
    <div class="panel panel-default">
        <div class="panel-body">
            <a href="{{route('inventario.configuracion.bodegas.index')}}" class="btn btn-default">
                Volver
            </a>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">Actualizar bodega</div>
        <div class="panel-body">
            <div class="form-group">
                {!! Form::open(['route' => ['inventario.configuracion.bodegas.update',$bodega], 'method'=>'PUT']) !!}
                <div class="panel panel-default">
                    <div class="panel-body">

                        <div class="form-group">
                            {!! Form::label('nombre','* Nombre del tipo de bodegas') !!}
                            {!! Form::text('nombre',$bodega->nombre,['class' => 'form-control', 'placeholder' => 'Nombre del tipo de bodegas...', 'required']) !!}
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="form-group">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('habilitado',1,$bodega->habilitado) !!}
                                    Deshabilitada
                                </label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::submit('Editar',['class' => 'btn btn-primary'])!!}
                </div>

                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

