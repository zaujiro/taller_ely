@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Inventario
@endsection


@section('main-content')
    <div class="panel panel-default">
        <div class="panel-heading">Inventario</div>
        <div class="panel-body">

            <!-- BUSCADOR DE TAGS -->
            {!! Form::open(['route' => 'inventario.configuracion.bodegas.index', 'method' => 'GET', 'class' => 'navbar-form pull-right']) !!}
            <div class="input-group">
                {!! Form::text('buscar',null, ['class' => 'form-control', 'placeholder' => 'buscar bodega...', 'aria-describedby' => 'search']) !!}
                <span class="input-group-addon" id="search">
                    <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                </span>
            </div>
            {!! Form::close() !!}
        <!-- FIN DEL BUSCADOR -->

            @if(count($bodegas)>0)
                <table class="table table-stripped">
                    <thead>
                    <th>Nombre</th>
                    <th></th>
                    </thead>

                    <tbody>
                    @foreach($bodegas As $bodega)

                        <tr>
                            <td>{{$bodega->nombre}}</td>
                            <td>
                                <div class="text-center">
                                    <a href="{{route('inventario.bodega',$bodega->id)}}"
                                       class="btn btn-default">
                                        <span class="glyphicon glyphicon-th-large" aria-hidden="true"></span>
                                    </a>
                                </div>

                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {!! $bodegas->render() !!}
            @else
                <hr>
                <p class="bg-warning text-center">
                    No se han registrado bodegas
                </p>
            @endif
        </div>
    </div>
@endsection


