@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Inventario
@endsection


@section('main-content')

    <div class="panel panel-default">
        <div class="panel-body">
            <a href="{{route('inventario.index')}}" class="btn btn-default">
                Volver
            </a>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">Inventario</div>
        <div class="panel-body">

            <!-- BUSCADOR DE TAGS -->
            {{--            {!! Form::open(['route' => 'inventario.configuracion.bodegas.index', 'method' => 'GET', 'class' => 'navbar-form pull-right']) !!}--}}
            <div class="input-group">
                {!! Form::text('buscar',null, ['class' => 'form-control', 'placeholder' => 'buscar producto...', 'aria-describedby' => 'search']) !!}
                <span class="input-group-addon" id="search">
                    <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                </span>
            </div>
            {{--            {!! Form::close() !!}--}}
        <!-- FIN DEL BUSCADOR -->

            @if(count($inventarios)>0)
                <table class="table table-bordered">
                    <thead>
                    <th>Producto</th>
                    <th>Marca</th>
                    <th>Material</th>
                    <th class="text-center">Cantidad en inventario</th>
                    </thead>

                    <tbody>
                    @foreach($inventarios As $inventario)

                        <tr>
                            <td>{{$inventario->ProductoGeneral->Producto->nombre}}</td>
                            <td>{{$inventario->ProductoGeneral->Marca->nombre}}</td>
                            <td>{{$inventario->ProductoGeneral->Material->nombre}}</td>
                            <td class="text-center">{{$inventario->cantidad}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {!! $inventarios->render() !!}
            @else
                <hr>
                <p class="bg-warning text-center">
                    No se han registrado bodegas
                </p>
            @endif
        </div>
    </div>
@endsection


