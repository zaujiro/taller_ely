@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Resolución
@endsection


@section('main-content')
    <div class="panel panel-default">
        <div class="panel-heading">Resolución</div>
        <div class="panel-body">

            @if($crear)
                <a href="{{ route('resolucion.create') }}" class="btn btn-info">Nueva resolución</a>
            @else
                <a href="#" class="btn btn-info" disabled>Nueva resolución</a>
            @endif
        <!-- BUSCADOR DE TAGS -->
            {!! Form::open(['route' => 'resolucion.index', 'method' => 'GET', 'class' => 'navbar-form pull-right']) !!}
            <div class="input-group">
                {!! Form::text('buscar',null, ['class' => 'form-control', 'placeholder' => 'busccar resolución...', 'aria-describedby' => 'search']) !!}
                <span class="input-group-addon" id="search">
                    <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                </span>
            </div>
            {!! Form::close() !!}
        <!-- FIN DEL BUSCADOR -->

            @if(count($resoluciones)>0)
                <table class="table table-stripped">
                    <thead>
                    <th>Resolución</th>
                    <th>Fecha Resolución</th>
                    <th>Valida Desde</th>
                    <th>Valida Hasta</th>
                    <th>Interés Corriente</th>
                    <th>Cerrada</th>
                    <th></th>
                    </thead>

                    <tbody>
                    @foreach($resoluciones As $resolucion)
                        <tr>
                            <td>{{$resolucion->numero_resolucion}}</td>
                            <td>{{$resolucion->fecha_resolucion}}</td>
                            <td>{{$resolucion->valida_desde}}</td>
                            <td>{{$resolucion->valida_hasta}}</td>
                            <td>{{$resolucion->interes_corriente}}%</td>
                            <td>{{$resolucion->cerrada}}</td>
                            <td>
                                <a href="{{route('resolucion.edit',$resolucion->id)}}" class="btn btn-warning">
                                    <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                                </a>
                                @if($crear)
                                    <a href="{{route('resolucion.destroy',$resolucion->id)}}"
                                       onclick=" return confirm('Desea eliminar esta resolución?')"
                                       class="btn btn-danger">
                                        <span class="glyphicon glyphicon-remove" aria-hidden="true">
                                        </span>
                                    </a>
                                @else
                                    <a href="#"
                                       class="btn btn-danger" disabled>
                                        <span class="glyphicon glyphicon-remove" aria-hidden="true">
                                        </span>
                                    </a>
                                @endif

                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

                {!! $resoluciones->render() !!}
            @else
                <hr>
                <p class="bg-warning text-center">
                    No se han registrado resoluciones.
                </p>
            @endif
        </div>
    </div>
@endsection


