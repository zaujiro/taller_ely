@extends('adminlte::layouts.app')
@section('htmlheader_title')
    liquidación
@endsection


@section('main-content')
    <div class="panel panel-default">
        <div class="panel-heading">Nueva Liquidación</div>
        <div class="panel-body">
            <div class="form-group">
                {!! Form::open(['route' => 'liquidacion.crear', 'method' => 'POST']) !!}

                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="form-group">
                            {!! Form::label('nombre','Tipo de Cálculo') !!}
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('plazo',1) !!}
                                    Plazo
                                </label>

                            </div>
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('mora',1) !!}
                                    Mora
                                </label>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="form-group">
                            {!! Form::label('monto','Monto: ') !!}
                            {!! Form::number('monto',null,['class' => 'form-control', 'placeholder' => 'Monto', 'required']) !!}
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-body">
                        {!! Form::label('mensaje','Ingrese el rango de fechas: ') !!}
                        <div class="form-group">
                            <div class="col">
                                <div class="col-xs-4 col-md-3">
                                    {!! Form::label('fecha','Desde: ') !!}
                                    {!! Form::date('fechaDesde',null,['class' => 'form-control', 'placeholder' => 'Desde', 'required']) !!}
                                </div>

                                <div class="col-xs-4 col-md-3">
                                    {!! Form::label('fecha','Hasta: ') !!}
                                    {!! Form::date('fechaHasta',null,['class' => 'form-control', 'placeholder' => 'Hasta', 'required']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::submit('Calcular',['class' => 'btn btn-primary'])!!}
                </div>

                {!! Form::close() !!}
            </div>

        </div>
    </div>
@endsection
@section('js')
    <script>
        $('.datepicker').datepicker({
            format: 'mm/dd/yyyy',
            startDate: '-3d'
        });
    </script>
@endsection