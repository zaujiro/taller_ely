@section('htmlheader_title')
    {{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
    <div class="panel panel-default">
        <div class="panel-heading">Nueva resolución</div>
        <div class="panel-body">
            <div class="form-group">
                {!! Form::open(['route' => 'resolucion.store', 'method' => 'POST']) !!}

                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="form-group">
                            {!! Form::label('fecha_resolucion','Resolución valida: ') !!}

                            <div class="row">
                                <div class="col-xs-4 col-md-2">
                                    {!! Form::label('desde','Desde') !!}
                                </div>
                                <div class="col-xs-5 col-md-3">
                                    {!! Form::date('valida_desde',null,['class' => 'form-control', 'placeholder' => 'Ruta', 'required']) !!}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-4 col-md-2">
                                    {!! Form::label('hasta','Hasta') !!}
                                </div>
                                <div class="col-xs-5 col-md-3">
                                    {!! Form::date('valida_hasta',null,['class' => 'form-control', 'placeholder' => 'Ruta', 'required','step'=>'any']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="form-group">
                            {!! Form::label('numero_resolucion','Numero de la resolución') !!}
                            {!! Form::text('numero_resolucion',null,['class' => 'form-control', 'placeholder' => 'Número de la resolución', 'required']) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('fecha_resolucion','Fecha de la resolución') !!}
                            <div class="row">
                                <div class="col-xs-4 col-md-3">
                                    {!! Form::date('fecha_resolucion',null,['class' => 'form-control', 'placeholder' => 'fecha de resolución', 'required']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="form-group">
                            {!! Form::label('interes','Interés Corriente') !!}
                            {!! Form::text('interes_corriente',null,['class' => 'form-control', 'placeholder' => 'Interés corriente', 'required']) !!}
                        </div>
                    </div>
                </div>


                <div class="form-group">
                    {!! Form::submit('Registrar',['class' => 'btn btn-primary'])!!}
                </div>

                {!! Form::close() !!}
            </div>

        </div>
    </div>
@endsection
@section('js')
    <script>
        $('.datepicker').datepicker({
            format: 'mm/dd/yyyy',
            startDate: '-3d'
        });
    </script>
@endsection