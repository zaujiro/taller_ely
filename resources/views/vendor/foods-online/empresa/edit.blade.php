@extends('adminlte::layouts.app')

@section('htmlheader_title')
    {{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')

    <div class="panel panel-default">
        <div class="panel-body">
            <a href="{{route('empresa.index')}}" class="btn btn-default">
                Volver
            </a>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">Actualizar Organización</div>
        <div class="panel-body">
            <div class="form-group">

                {!! Form::open(['route' => ['empresa.update',$empresa], 'method'=>'PUT','files' => true]) !!}

                <div class="form-group">
                    {!! Form::label('nit','NIT') !!}
                    {!! Form::text('nit',$empresa->nit,['class' => 'form-control', 'placeholder' => 'NIT...', 'required']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('nombre','Nombre o razón social') !!}
                    {!! Form::text('nombre',$empresa->nombre,['class' => 'form-control', 'placeholder' => 'Nombre ó razón social', 'required']) !!}
                </div>

                <div class="form-group">
                    @if($empresa->logo!='')
                        <div class="col-xs-6 col-lg-3">
                            <img src="{{asset('img/foods-online/empresa/'.$empresa->logo)}}"
                                 class="img-responsive"
                                 alt="...">
                        </div>
                    @endif
                </div>

                <div class="form-group" style="padding-top: 150px">
                    {!! Form::label('logo','Logo') !!}
                    {!! Form::file('logo',['class' => 'form-control']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('telefono','Teléfono(s)') !!}
                    {!! Form::text('telefono',$empresa->telefono,['class' => 'form-control', 'placeholder' => 'Teléfono(s)...', 'required']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('celular','Celular') !!}
                    {!! Form::text('celular',$empresa->celular,['class' => 'form-control', 'placeholder' => 'Celular...', 'required']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('website','Website') !!}
                    {!! Form::text('website',$empresa->website,['class' => 'form-control', 'placeholder' => 'Website...', 'required']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('id_departamento','Departamento') !!}
                    {!! Form::select('id_departamento',$departamentos,$empresa->municipio->departamento->id,['class' => 'form-control select-padre', 'id'=>'select-departamento','placeholder' => 'seleccione una opción', 'required']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('id_municipio','Municipio') !!}

                    <div id="municipioDiv">
                        {!! Form::select('id_municipio',$municipios ,$empresa->municipio->id,['class' => 'form-control select-padre', 'id'=>'select-municipio','placeholder' => 'seleccione una opción', 'required']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::submit('Editar',['class' => 'btn btn-success'])!!}
                </div>

                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script>
        $('.select-padre').chosen({
            placeholder_text_single: "Selecciona una opción",
            no_results_text: "No se han encontrado "
        });
    </script>

    <!-- Llamado al script de las listas dependientes -->
    <script>
        $("#select-departamento").change(function () {
            onSelectDependiente('select-departamento', 'select-municipio', '../municipios/')
        });
    </script>
@endsection

