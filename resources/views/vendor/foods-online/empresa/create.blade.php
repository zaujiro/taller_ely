@extends('adminlte::layouts.app')

@section('htmlheader_title')
    {{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')

    <div class="panel panel-default">
        <div class="panel-body">
            <a href="{{route('empresa.index')}}" class="btn btn-default">
                Volver
            </a>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">Crear Organización</div>
        <div class="panel-body">
            <div class="form-group">
                {!! Form::open(['route' => 'empresa.store', 'method' => 'POST','files' => true]) !!}

                <div class="form-group">
                    {!! Form::label('nit','NIT') !!}
                    {!! Form::text('nit',null,['class' => 'form-control', 'placeholder' => 'NIT...', 'required']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('nombre','Nombre o razón social') !!}
                    {!! Form::text('nombre',null,['class' => 'form-control', 'placeholder' => 'Nombre ó razón social', 'required']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('logo','Logo') !!}
                    {!! Form::file('logo',['class' => 'form-control']) !!}

                </div>

                <div class="form-group">
                    {!! Form::label('telefono','Teléfono(s)') !!}
                    {!! Form::text('telefono',null,['class' => 'form-control', 'placeholder' => 'Teléfono(s)...', 'required']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('celular','Celular') !!}
                    {!! Form::text('celular',null,['class' => 'form-control', 'placeholder' => 'Celular...', 'required']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('website','Website') !!}
                    {!! Form::text('website',null,['class' => 'form-control', 'placeholder' => 'Website...', 'required']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('id_departamento','Departamento') !!}
                    {!! Form::select('id_departamento',$departamentos,null,['class' => 'form-control select-padre', 'id'=>'select-departamento','placeholder' => 'seleccione una opción', 'required']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('id_municipio','Municipio') !!}

                    <div id="municipioDiv">
                        {!! Form::select('id_municipio',[],null,['class' => 'form-control select-padre', 'id'=>'select-municipio','placeholder' => 'seleccione una opción', 'required']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::submit('Registrar',['class' => 'btn btn-primary'])!!}
                </div>

                {!! Form::close() !!}
            </div>

        </div>
    </div>
@endsection
@section('js')
    <script>
        $('.select-padre').chosen({
            placeholder_text_single: "Selecciona una opción",
            no_results_text: "No se han encontrado "
        });
    </script>

    <!-- Llamado al script de las listas dependientes -->
    <script>
        $("#select-departamento").change(function () {
            onSelectDependiente('select-departamento', 'select-municipio', 'municipios/')
        });
    </script>
@endsection

