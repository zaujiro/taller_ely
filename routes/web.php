<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

/**
 * Usamos el middleware 'auth' para comprobar que solo un usuario que se ha logueado podrá acceder
 * a las rutas
 */
Route::group(['middleware' => 'auth'], function () {

    /** ---------------------------- MENU -------------------------------- */
    /**
     * Para el manejo de los menús
     */
    Route::resource('menu', 'sistema\menu\MenuController');
    Route::get('menu/{id}/destroy', [
        'uses' => 'sistema\menu\MenuController@destroy',
        'as' => 'menu.destroy'
    ]);

    /** Ruta para los selects dependientes que consultan los hijos de un padre */
    Route::get('menu/hijos/{id}', [
        'uses' => 'sistema\menu\MenuController@getHijos',
        'as' => 'menu.hijos'
    ]);

    /** ---------------------------- ROLES -------------------------------- */
    /**
     * Para el manejo de los roles
     */
    Route::resource('rol', 'sistema\usuario\rol\RolController');
    Route::get('rol/{id}/destroy', [
        'uses' => 'sistema\usuario\rol\RolController@destroy',
        'as' => 'rol.destroy'
    ]);

    /** ---------------------------- USUARIOS -------------------------------- */
    /**
     * Para el manejo de los usuarios
     */
    Route::resource('user', 'sistema\usuario\usuario\UserController');
    Route::get('user/{id}/destroy', [
        'uses' => 'sistema\usuario\usuario\UserController@destroy',
        'as' => 'user.destroy'
    ]);

    /** ---------------------------- PERMISOS -------------------------------- */
    /**
     * Para el manejo de los permisos
     */
    Route::resource('permiso', 'sistema\usuario\permiso\PermisoController');
    Route::get('permiso/{id}/destroy', [
        'uses' => 'sistema\usuario\permiso\permisoController@destroy',
        'as' => 'permiso.destroy'
    ]);


    /** ---------------------------- EMPRESA -------------------------------- */
    /**
     * Para el manejo de los datos de la empresa
     */
    Route::resource('empresa', 'empresa\EmpresaController');
    Route::get('empresa/{id}/destroy', [
        'uses' => 'empresa\EmpresaController@destroy',
        'as' => 'empresa.destroy'
    ]);

    /**
     * Ruta que retorna una colección de municipios del departamento {id} que se reciba
     */
    Route::get('empresa/municipios/{id}', [
        'uses' => 'empresa\EmpresaController@getMunicipios',
        'as' => 'empresa.municipios'
    ]);
    /** ---------------------------- ALMACEN - CONFIGURACION - MARCAS -------------------------------- */
    /**
     * Para la configuración de los tipos de análisis
     */
    Route::resource('almacen/configuracion/marcas', 'almacen\configuracion\productos\marcas\MarcaController', ['names' => [
        'index' => 'almacen.configuracion.orden.marcas.index',
        'create' => 'almacen.configuracion.orden.marcas.create',
        'store' => 'almacen.configuracion.orden.marcas.store',
        'edit' => 'almacen.configuracion.orden.marcas.edit',
        'update' => 'almacen.configuracion.orden.marcas.update',
        'show' => 'almacen.configuracion.orden.marcas.show',
        'destroy' => 'almacen.configuracion.orden.marcas.destroy',
    ]]);

    /**
     * para eliminar marca
     */
    Route::get('almacen/configuracion/orden/marca/{id}/destroy', [
        'uses' => 'almacen\configuracion\productos\marcas\MarcaController@destroy',
        'as' => 'almacen.configuracion.orden.marcas.destroy'
    ]);


    /** ------------------------------------------------------------------------------------ */

    /** ---------------------------- Almacen Productos ------------------------------------- */

    Route::resource('almacen/configuracion/orden', 'almacen\configuracion\productos\productos\ProductoController',
        ['names' => [
            'index' => 'almacen.configuracion.orden.orden.index',
            'create' => 'almacen.configuracion.orden.orden.create',
            'store' => 'almacen.configuracion.orden.orden.store',
            'edit' => 'almacen.configuracion.orden.orden.edit',
            'update' => 'almacen.configuracion.orden.orden.update',
            'show' => 'almacen.configuracion.orden.orden.show',
            'destroy' => 'almacen.configuracion.orden.orden.destroy',
        ]]);

    /**
     * Ruta para eliminar un producto
     */
    Route::get('almacen/configuracion/orden/{id}/destroy', [
        'uses' => 'almacen\configuracion\productos\productos\ProductoController@destroy',
        'as' => 'almacen.configuracion.orden.orden.destroy'
    ]);

    /** ---------------------------- Almacen Materiales ------------------------------------- */

    Route::resource('almacen/configuracion/materiales', 'almacen\configuracion\productos\materiales\MaterialController',
        ['names' => [
            'index' => 'almacen.configuracion.orden.materiales.index',
            'create' => 'almacen.configuracion.orden.materiales.create',
            'store' => 'almacen.configuracion.orden.materiales.store',
            'edit' => 'almacen.configuracion.orden.materiales.edit',
            'update' => 'almacen.configuracion.orden.materiales.update',
            'show' => 'almacen.configuracion.orden.materiales.show',
            'destroy' => 'almacen.configuracion.orden.materiales.destroy',
        ]]);

    /**
     * Ruta para eliminar un material
     */
    Route::get('almacen/configuracion/materiales/{id}/destroy', [
        'uses' => 'almacen\configuracion\productos\materiales\MaterialController@destroy',
        'as' => 'almacen.configuracion.orden.materiales.destroy'
    ]);

    /** ---------------------------- Almacen Texturas ------------------------------------- */

    Route::resource('almacen/configuracion/texturas', 'almacen\configuracion\productos\texturas\TexturaController',
        ['names' => [
            'index' => 'almacen.configuracion.orden.texturas.index',
            'create' => 'almacen.configuracion.orden.texturas.create',
            'store' => 'almacen.configuracion.orden.texturas.store',
            'edit' => 'almacen.configuracion.orden.texturas.edit',
            'update' => 'almacen.configuracion.orden.texturas.update',
            'show' => 'almacen.configuracion.orden.texturas.show',
            'destroy' => 'almacen.configuracion.orden.texturas.destroy',
        ]]);

    /**
     * Ruta para eliminar una texturas
     */
    Route::get('almacen/configuracion/texturas/{id}/destroy', [
        'uses' => 'almacen\configuracion\productos\texturas\TexturaController@destroy',
        'as' => 'almacen.configuracion.orden.texturas.destroy'
    ]);


    /** ---------------------------- Almacen Proveedores ------------------------------------- */

    Route::resource('almacen/configuracion/proveedores', 'almacen\configuracion\proveedores\proveedores\ProveedorController',
        ['names' => [
            'index' => 'almacen.configuracion.proveedores.index',
            'create' => 'almacen.configuracion.proveedores.create',
            'store' => 'almacen.configuracion.proveedores.store',
            'edit' => 'almacen.configuracion.proveedores.edit',
            'update' => 'almacen.configuracion.proveedores.update',
            'show' => 'almacen.configuracion.proveedores.show',
            'destroy' => 'almacen.configuracion.proveedores.destroy',
        ]]);

    /**
     * Ruta para eliminar un proveedor
     */
    Route::get('almacen/configuracion/proveedores/{id}/destroy', [
        'uses' => 'almacen\configuracion\proveedores\proveedores\ProveedorController@destroy',
        'as' => 'almacen.configuracion.proveedores.destroy'
    ]);


    /** ---------------------------- ALMACEN - CONFIGURACIÓN - PRODUCTOS - GENERAL ------------------------------------- */

    Route::resource('almacen/configuracion/general/productos', 'almacen\configuracion\productos\general\ProductosGeneralController',
        ['names' => [
            'index' => 'almacen.configuracion.general.productos.index',
            'create' => 'almacen.configuracion.general.productos.create',
            'store' => 'almacen.configuracion.general.productos.store',
            'edit' => 'almacen.configuracion.general.productos.edit',
            'update' => 'almacen.configuracion.general.productos.update',
            'show' => 'almacen.configuracion.general.productos.show',
            'destroy' => 'almacen.configuracion.general.productos.destroy',
        ]]);

    /** Ruta para retornar la colección de materiales de una marca seleccionada  */

    Route::get('almacen/configuracion/general/productos/materiales/{producto_id}/{marca_id}', [
        'uses' => 'almacen\configuracion\productos\general\ProductosGeneralController@getMateriales',
        'as' => 'almacen.configuracion.general.orden.materiales'
    ]);

    /**
     * Ruta para eliminar orden generales
     */
    Route::get('almacen/configuracion/general/orden/{id}/destroy', [
        'uses' => 'almacen\configuracion\productos\general\ProductosGeneralController@destroy',
        'as' => 'almacen.configuracion.general.orden.destroy'
    ]);

    /**
     * Ruta para habilitar orden generales
     */
    Route::get('almacen/configuracion/general/orden/{id}/habilitar', [
        'uses' => 'almacen\configuracion\productos\general\ProductosGeneralController@habilitar',
        'as' => 'almacen.configuracion.general.orden.habilitar'
    ]);


    /** ---------------------------- Almacen - Configuracion -  Insumos ------------------------------------- */

    Route::resource('almacen/configuracion/insumos', 'almacen\configuracion\insumos\InsumoController',
        ['names' => [
            'index' => 'almacen.configuracion.insumos.index',
            'create' => 'almacen.configuracion.insumos.create',
            'store' => 'almacen.configuracion.insumos.store',
            'edit' => 'almacen.configuracion.insumos.edit',
            'update' => 'almacen.configuracion.insumos.update',
            'show' => 'almacen.configuracion.insumos.show',
            'destroy' => 'almacen.configuracion.insumos.destroy',
        ]]);

    Route::get('almacen/configuracion/insumos/{id}/destroy', [
        'uses' => 'almacen\configuracion\insumos\InsumoController@destroy',
        'as' => 'almacen.configuracion.insumos.destroy'
    ]);

    /** ----------------------------  Orden - Compras -  Insumos ------------------------------------- */

    Route::resource('compras/insumos', 'almacen\compras\insumos\OrdenCompraInsumoController',
        ['names' => [
            'index' => 'almacen.compras.insumos.index',
            'create' => 'almacen.compras.insumos.create',
            'store' => 'almacen.compras.insumos.store',
            'edit' => 'almacen.compras.insumos.edit',
            'update' => 'almacen.compras.insumos.update',
            'show' => 'almacen.compras.insumos.show',
            'destroy' => 'almacen.compras.insumos.destroy',
        ]]);

    Route::get('compras/insumos/{id}/destroy', [
        'uses' => 'almacen\compras\insumos\OrdenCompraInsumoController@destroy',
        'as' => 'almacen.compras.insumos.destroy'
    ]);


    /** ----------------------------  Almacen - Configuracion - Proveedeores - Productos ------------------------------------- */

    Route::resource('almacen/configuracion/productosproveedor',
        'almacen\configuracion\proveedores\productos\ProductoProveedorController',
        ['names' => [
            'index' => 'almacen.configuracion.productosproveedor.index',
            'create' => 'almacen.configuracion.productosproveedor.create',
            'store' => 'almacen.configuracion.productosproveedor.store',
            'edit' => 'almacen.configuracion.productosproveedor.edit',
            'update' => 'almacen.configuracion.productosproveedor.update',
            'show' => 'almacen.configuracion.productosproveedor.show',
            'destroy' => 'almacen.configuracion.productosproveedor.destroy',
        ]]);

    Route::get('almacen/configuracion/productosproveedor/{id}/destroy',
        [
            'uses' => 'almacen\configuracion\proveedores\productos\ProductoProveedorController@destroy',
            'as' => 'almacen.configuracion.productosproveedor.destroy'
        ]);

    /** ---------------------------- COMPRAS - ORDENES _PRODUCTOS ------------------------------------- */
    Route::resource('compras/ordenes/', 'almacen\compras\productos\OrdenCompraProductoController',
        ['names' => [
            'index' => 'compras.ordenes.index',
            'create' => 'compras.ordenes.create',
            'store' => 'compras.ordenes.store',
            'edit' => 'compras.ordenes.edit',
            'update' => 'compras.ordenes.update',
            'show' => 'compras.ordenes.show',
            'destroy' => 'compras.ordenes.destroy',
        ]]);

    /**-------------- Productos de un proveedor ------------------------*/
    Route::get('compras/ordenes/productos/{id}', [
        'uses' => 'almacen\compras\productos\OrdenCompraProductoController@getProductosProveedor',
        'as' => 'compras.ordenes.proveedor.productos'
    ]);

    /**-------------- PDF orden de compra ------------------------*/
    Route::get('compras/ordenes/pdf/{id}', [
        'uses' => 'almacen\compras\productos\OrdenCompraProductoController@ordenPdf',
        'as' => 'compras.ordenes.pdf'
    ]);
    /**-------------- Eliminar orden de compra ------------------------*/
    Route::get('compras/ordenes/{id}/destroy', [
        'uses' => 'almacen\compras\productos\OrdenCompraProductoController@destroy',
        'as' => 'compras.ordenes.destroy'
    ]);
    /** ---------------------------- COMPRAS - ORDENES _PRODUCTOS -RECIBIR ------------------------------------- */

    /**-------------- Lista de ordenes de compra para recibir  ------------------------*/
    Route::get('compras/ordenes/recibir', [
        'uses' => 'almacen\compras\productos\OrdenCompraProductoController@mostrarRecibir',
        'as' => 'compras.ordenes.recibir.index'
    ]);

    /**-------------- Formulario para recibir una orden de compra ------------------------*/
    Route::get('compras/ordenes/recibir/{id}', [
        'uses' => 'almacen\compras\productos\OrdenCompraProductoController@formularioRecibir',
        'as' => 'compras.ordenes.recibir.formulario'
    ]);

    /**-------------- Guardar los productos que se reciben de una orden de compra ------------------------*/
    Route::put('compras/ordenes/recibir/{id}/save', [
        'uses' => 'almacen\compras\productos\OrdenCompraProductoController@guardarRecibidoOrden',
        'as' => 'compras.ordenes.recibir.save'
    ]);

    /** ---------------------------- INVENTARIO - CONFIGURACIÓN - BODEGAS------------------------------------- */
    /**
     * Para la configuración de las bodegas de inventario
     */
    Route::resource('inventario/configuracion/bodegas', 'inventario\configuracion\BodegaController', ['names' => [
        'index' => 'inventario.configuracion.bodegas.index',
        'create' => 'inventario.configuracion.bodegas.create',
        'store' => 'inventario.configuracion.bodegas.store',
        'edit' => 'inventario.configuracion.bodegas.edit',
        'update' => 'inventario.configuracion.bodegas.update',
        'show' => 'inventario.configuracion.bodegas.show',
        'destroy' => 'inventario.configuracion.bodegas.destroy',
    ]]);
    /** -------- habilitar bodega -------- */
    Route::get('inventario/configuracion/bodegas/{id}/habilitar', [
        'uses' => 'inventario\configuracion\BodegaController@habilitar',
        'as' => 'inventario.configuracion.bodegas.habilitar'
    ]);

    /** -------- inhabilitar bodega-------- */
    Route::get('inventario/configuracion/bodegas/{id}/destroy', [
        'uses' => 'inventario\configuracion\BodegaController@destroy',
        'as' => 'inventario.configuracion.bodegas.destroy'
    ]);

    /** ---------------------------- INVENTARIO ------------------------------------- */

    /** -------- Inventario movimientos -------- */
    Route::get('inventario/movimiento', [
        'uses' => 'inventario\InventarioController@movimiento',
        'as' => 'inventario.movimiento'
    ]);

    /** -------- Inventario guardar movimientos -------- */
    Route::put('inventario/save', [
        'uses' => 'inventario\InventarioController@guardarMovimiento',
        'as' => 'inventario.movimiento.save'
    ]);

    /** -------- Vista bodegas de inventario -------- */
    Route::get('inventario', [
        'uses' => 'inventario\InventarioController@index',
        'as' => 'inventario.index'
    ]);
    /** -------- Inventario de una bodega -------- */
    Route::get('inventario/{id}', [
        'uses' => 'inventario\InventarioController@inventario',
        'as' => 'inventario.bodega'
    ]);



    //Please do not remove this if you want adminlte:route and adminlte:link commands to works correctly.
    #adminlte_routes
});