/**
 * Funciòn que suma todos los valores de una clase y cambia la etiqueta de la misma
 *
 * Se tiene un div grupo que contiene todos los campos que van a cambiar
 */
$(".grupo").keyup(function () {

    /** Obtenemos el id del grupo que debe ser ùnico por grupo **/
    var id = $(this).attr("id");

    /** obtenemos el precio de el grupo **/
    var precio = $("#precio"+id).val();

    /** Cantidad inicial predeterminada **/
    cantidad = 0;

    /** Seleccionamos todos los campos cantidad del grupo con el id seleccionado **/
    $(".cantidad"+id).each(
        function (index, value) {
            /** Si el campo tiene algùn valor lo sumamos **/
            if($(this).val()){
                cantidad += eval($(this).val());
            }
        }
    );

    /** Cambiamos el valor de la etiqueta total para tener en cuenta la cantidad total**/
    $("#cantidadTotal"+id).html(cantidad);

    /** Cambiamos el valor del campo oculto que contiene el valor del precio*cantidad **/
    $("#precioXcantidad"+id).val(cantidad*precio);

    /** Cambiamos el valor del html del label que dirá el total del valor del grupo **/
    $("#labelTotalProducto"+id).html("$" + number_format(cantidad*precio));


    /** total general inicial **/
    var total = 0;
    /** Leemos todos los campos que contiene precio*cantidad para sumar sus valores y
     * cambiar el valor de la etiqueta del total
     **/
    $(".precioXcantidad").each(
        function (index, value) {
            /** Si el campo tiene algùn valor lo sumamos **/
            if($(this).val()){
                total += eval($(this).val());
            }
        }
    );

    /** Cambiamos el valor del html del label que dirá el total del valor del grupo **/
    $("#total-general").html("$" + number_format(total));
});