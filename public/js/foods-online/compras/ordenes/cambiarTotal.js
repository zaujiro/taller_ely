/**
 * Función que cambia la etiqueta del total(precio*cantidad)
 * @param campoCantidadId el id del campo cantidad
 * @param campoTotalId Campo donde se guarda el valor precio*cantidad para tomar y sumar todos los valores
 * @param labelTotalId el label donde pondrá el resultado
 * @param precio el precio por el que debe multiplicar
 * @param labelSumaTotal label donde se pondrá la suma total
 */
function cambiarTotal(campoCantidadId, campoTotalId,labelTotalId, precio, labelSumaTotal) {
    /** Obtenemos el valor del campo cantidad**/
    var campoCantidad = document.getElementById(campoCantidadId).value;
    /** Variable que contiene el valor de la etiqueta del total**/
    var valorEtiquetaTotal = null;
    var valorCampoTotal = 0;

    /** Comprobamos que se escribiera algo en el campo cantidad**/
    if (campoCantidad) {
        valorEtiquetaTotal = "$ " + number_format(campoCantidad * precio);
        valorCampoTotal = campoCantidad * precio;
    }
    else {
        valorEtiquetaTotal = "-";
        valorCampoTotal = 0;
    }

    /** Cambiamos el valor de la etiqueta del total con el que tenga la variable **/
    document.getElementById(labelTotalId).innerHTML = valorEtiquetaTotal;
    document.getElementById(campoTotalId).value = valorCampoTotal;
    /** Hacemos el llamado a la función sumaTotal**/
    sumaTotal(labelSumaTotal);
}

/**
 * Función que suma todos los valores de una clase html
 * @param camposSumar el nombre de la clase de la que tomará los valores
 */
function sumaTotal(camposSumar) {

    importe_total = 0;

    /**
     * Leemos cada elemento que tiene el nombre de la clase y sumamos el valor que tengan
     */
    $("." + camposSumar).each(
        function (index, value) {
            importe_total += eval($(this).val());
        }
    );

    /** Cambiamos el valor del html del label que dirá el total del valor **/
    $("#"+camposSumar).html("$ " + number_format(importe_total));
}